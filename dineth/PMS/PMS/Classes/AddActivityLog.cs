﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PMS
{
    public static class AddActivityLog
    {
        public static void AddActivity(string action)
        {

            string appPath = @"./Activity/";
            string fileName = "PMS_" + DateTime.Now.ToString("M-dd-yyyy") + ".txt";
            string filePath = appPath + fileName;

            if (System.IO.File.Exists(filePath) == true)
            {
                StreamWriter sw = new StreamWriter(filePath, true);
                sw.WriteLine(action + "\t" + System.DateTime.Now.ToString() + "\t" + UserDetails.UserName + "\n");
                sw.Flush();
                sw.Close();

            }
            else
            {
                StreamWriter w;
                w = File.CreateText(filePath);
                w.WriteLine(action + "\t" + System.DateTime.Now.ToString() + "\t" + UserDetails.UserName + "\n");
                w.Flush();
                w.Close();
            }
        }
    }
}
