﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
	public partial class ViewDeclaration
	{
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        private Declaration declaration;
		public ViewDeclaration(Declaration declaration)
		{
			this.InitializeComponent();
            this.declaration = declaration;

			// Insert code required on object creation below this point.
		}
        

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.Load_Declaration_Details();

        }

        private void Load_Declaration_Details()
        {
            lbl_buyer.Content = declaration.Buyer.Name;
            lbl_commodity.Content = declaration.Commodity.Name;
            lbl_country.Content = declaration.Country.Name;
            lbl_credit_duration.Content = declaration.CreditDuration;
            lbl_exporter.Content = declaration.Exporter.Name;
            lbl_gross_value.Content = declaration.GrossValue;
            lbl_payment_term.Content = declaration.TermOfPayment;
            lbl_policy.Content = declaration.Policy.PolicyNumber;
            lbl_shipment_date.Content = declaration.ShipmentDate.ToShortDateString();
            lbl_stamp_date.Content = declaration.StampDate.ToShortDateString();


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).frame2.Content = new EditDeclaration(declaration.Id);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var dec = model.Declarations.Where(i => i.Id == declaration.Id).FirstOrDefault();
            model.Declarations.DeleteObject(dec);
            model.SaveChanges();
            ((SearchDeclaration)((MainWindow)Application.Current.MainWindow).frame1.Content).fillGrid();
           
        }
	}
}