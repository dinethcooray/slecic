﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.Threading;

namespace PMS.GUI
{
    /// <summary>
    /// Interaction logic for EmailSender.xaml
    /// </summary>
    public partial class EmailSender : Page
    {

        PMS_Main_ModelContainer model;
        string filename;
        Nullable<bool> result;
        Boolean status;
        private delegate void UpdateProgressBarDelegate(System.Windows.DependencyProperty dp, Object value);
        private delegate void testdelegate();
        public EmailSender()
        {
            InitializeComponent();
            model = new PMS_Main_ModelContainer();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Thread MyThread = new Thread(new ThreadStart (MyCallbackFunction));
           // MyThread.Start();
        //   Process();
            SendEmails();



        }
        public String[] spliter(char c)
        {
            string emails = txtEmail.Text;

            string[] token = emails.Split(c);

            return token;


        }



        public void SendEmails()
        {
            status = false;
            MailMessage mail = new MailMessage();
            SmtpClient smtps = new SmtpClient();
            mail.From = new MailAddress("xtesting89@gmail.com");

            String[] email = spliter(',');

            foreach (String s in email)
                mail.To.Add(new MailAddress(s));


            mail.Subject = txtSubject.Text;
            mail.Body = txtMessageBody.Text;
            if (lblAttachfile.Content != null && checkfile.IsChecked == true)
            {
                Attachment attfile = new Attachment(lblAttachfile.Content.ToString());
                mail.Attachments.Add(attfile);
            }


            smtps.Host = "smtp.gmail.com";
            smtps.Port = 587;
            smtps.Credentials = new NetworkCredential("xtesting89@gmail.com", "xtesting20122013test");
            smtps.EnableSsl = false; // runtime encrypt the SMTP communications using SSL
            try
            {

                //Thread MyThread = new Thread(new ThreadStart (MyCallbackFunction));
                //MyThread.Start();
                smtps.Send(mail);
                status = true;
                if (status)
                {
                    //MyThread.Abort();
                    MessageBox.Show("Email was successfully sent to " + txtEmail.Text, "Sent");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured retry again", "Error");
            }


        }







        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            lblfile.Visibility = System.Windows.Visibility.Hidden;
            checkfile.Visibility = System.Windows.Visibility.Hidden;



        }




        private void BindCompanyBuyerEmails()
        {

            IEnumerable<CompanyBuyer> companybuyer = model.Buyers.OfType<CompanyBuyer>();

            comboxEmail.ItemsSource = companybuyer;
            comboxEmail.DisplayMemberPath = "Name";
        }




        private void BindIndividualBuyerEmails()
        {

            IEnumerable<IndividualBuyer> individualBuyer = model.Buyers.OfType<IndividualBuyer>();
            //  var emails = companybuyer.Select(i=>i.Id
            comboxEmail.ItemsSource = individualBuyer;
            comboxEmail.DisplayMemberPath = "Name";
        }

        private void BindexporterEmails()
        {

            IEnumerable<Exporter> exporter = model.Exporters;
            comboxEmail.ItemsSource = exporter;
            comboxEmail.DisplayMemberPath = "Name";
        }



        private void radBuyer_Checked(object sender, RoutedEventArgs e)
        {
            // comboxEmail.Items.Clear();
            BindCompanyBuyerEmails();
        }



        private void radExport_Checked(object sender, RoutedEventArgs e)
        {
            BindexporterEmails();
        }

        private void comboxEmail_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            if (radBuyer.IsChecked == true)
            {
                if (comboxEmail.SelectedItem != null)
                {
                    CompanyBuyer companybuyer;

                    companybuyer = (CompanyBuyer)comboxEmail.SelectedItem;
                    txtEmail.Text = txtEmail.Text + companybuyer.Email + ",";
                }
            }

            if (radindividual.IsChecked == true)
            {
                if (comboxEmail.SelectedItem != null)
                {
                    IndividualBuyer individualbuyer; ;

                    individualbuyer = (IndividualBuyer)comboxEmail.SelectedItem;
                    txtEmail.Text = txtEmail.Text + individualbuyer.Email + ",";
                }
            }

            if (radexport.IsChecked == true)
            {
                if (comboxEmail.SelectedItem != null)
                {
                    Exporter exporter; ;

                    exporter = (Exporter)comboxEmail.SelectedItem;
                    txtEmail.Text = txtEmail.Text + exporter.Email + ",";
                }
            }
        }

        private void radIndividual_Checked(object sender, RoutedEventArgs e)
        {
            BindIndividualBuyerEmails();
        }

        private void btnAttach_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            // dlg.DefaultExt = ".txt"; // Default file extension
            // dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Show open file dialog box
            result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                filename = dlg.FileName;
                lblAttachfile.Content = filename;
                lblfile.Visibility = System.Windows.Visibility.Visible;
                checkfile.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void checkfile_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void checkfile_Click(object sender, RoutedEventArgs e)
        {
            if (checkfile.IsChecked == false)
            {
                lblfile.Visibility = System.Windows.Visibility.Hidden;
                checkfile.Visibility = System.Windows.Visibility.Hidden;
                lblAttachfile.Content = "";
            }

        }



        public static void MyCallbackFunction()
        {

            //ProgressBar pb = new ProgressBar();
            //pb.VerticalAlignment =System.Windows.VerticalAlignment.Center;
            //pb.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            //pb.Width = 70;
            //pb.Height = 25;
            //pb.Value = 50;
            MessageBox.Show("test");


        }



        private void Process()
        {
            //Configure the ProgressBar
            progressBar1.Minimum = 0;
            progressBar1.Maximum = short.MaxValue;
            progressBar1.Value = 0;

            //Stores the value of the ProgressBar
            double value = 0;

            //Create a new instance of our ProgressBar Delegate that points
            // to the ProgressBar's SetValue method.
            UpdateProgressBarDelegate updatePbDelegate =
                new UpdateProgressBarDelegate(progressBar1.SetValue);


            
            //Tight Loop: Loop until the ProgressBar.Value reaches the max
            do
            {
                value += 1;

                /*Update the Value of the ProgressBar:
                    1) Pass the "updatePbDelegate" delegate
                       that points to the ProgressBar1.SetValue method
                    2) Set the DispatcherPriority to "Background"
                    3) Pass an Object() Array containing the property
                       to update (ProgressBar.ValueProperty) and the new value */
               // Dispatcher.Invoke(updatePbDelegate,
                   // System.Windows.Threading.DispatcherPriority.Background,
                   // new object[] { ProgressBar.ValueProperty, value });
            }
            while (progressBar1.Value != progressBar1.Maximum);
        }



    }


}       
  

