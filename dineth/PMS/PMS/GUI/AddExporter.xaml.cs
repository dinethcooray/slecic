﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for AddExporter.xaml
    /// </summary>
    public partial class AddExporter : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Exporter exporter;
        public AddExporter()
        {
            InitializeComponent();
            exporter = new Exporter();

            //fill combo boxes
            txtStatus.ItemsSource = model.ExporterStatus;
            txtStatus.DisplayMemberPath = "Description";
            txtStatus.SelectedItem = 1;
        }

        public AddExporter(int ExpId)
        { 
            //create a exporter object if the exp id is given
           exporter=(Exporter)model.Exporters.Where(exp=>exp.Id==ExpId).First();
           txtName.Text = exporter.Name;
           txtRegNo.Text = exporter.RegNo;
           txtShortTag.Text=exporter.ShortTag;
           txtTelNo.Text = exporter.TelNo;
           txtEmail.Text = exporter.Email;
           txtDescription.Text = exporter.Description;
           txtAddress.Text = exporter.Address;
            

        }

        private void button_Save_Click(object sender, RoutedEventArgs e)
        {
            Validation validation = new Validation();
            IEnumerable<Exporter> exporters = model.Exporters.Where(exp => exp.RegNo == txtRegNo.Text);
            //IEnumerable<User> users = model.Users.Where(user => user.UserName == txtUserName.Text);
            if (exporters.Count() == 0)
            {
                if(txtStatus.SelectedValue!=null)
                {
                if(txtAddress.Text!=null)
                {
                if(txtRegNo.Text!=null)
                {
                if (txtName.Text != null)
                {
                    //validation for emails
                    if (validation.ValidateTextField(txtEmail.Text) ? validation.ValidateEmail(txtEmail.Text) : true)
                    {

                        exporter.Name = txtName.Text;
                        exporter.RegNo = txtRegNo.Text;
                        exporter.TelNo = txtTelNo.Text;
                        exporter.ShortTag = txtShortTag.Text;
                        exporter.Email = txtEmail.Text;
                        exporter.Description = txtDescription.Text;
                        exporter.Address = txtAddress.Text;
                        exporter.ExporterStatu = (ExporterStatus)txtStatus.SelectedItem;
                        //adding to data model
                        model.AddToExporters(exporter);
                        //applying changes to the database
                        model.SaveChanges();
                        MessageBox.Show("Exporter added successfully", "Added");

                        //clearing fields
                        txtAddress.Clear();
                        txtDescription.Clear();
                        txtEmail.Clear();
                        txtRegNo.Clear();
                        txtShortTag.Clear();
                        txtStatus.ClearValue(ComboBox.SelectedItemProperty);
                        txtTelNo.Clear();
                        txtName.Clear();




                    }
                    else
                    {
                        MessageBox.Show("Invalid Email");
                    }
                }
                else {
                    MessageBox.Show("Please enter a name");
                    
                }
                }
                else
                {
                    MessageBox.Show("Please enter a Register number");

                }
                }
                else
                {
                    MessageBox.Show("Please enter an address");

                }
                }
                else
                {
                    MessageBox.Show("Please select a buyer status");

                }
            }
        }

        private void txtStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void button_clear_Click(object sender, RoutedEventArgs e)
        {
            txtAddress.Clear();
            txtDescription.Clear();
            txtEmail.Clear();
            txtName.Clear();
            txtRegNo.Clear();
            txtShortTag.Clear();
            txtStatus.Text = null;
            txtTelNo.Clear();
            
        }
    }
}
