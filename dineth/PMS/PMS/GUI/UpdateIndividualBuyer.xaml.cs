﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for UpdateIndividualBuyer.xaml
    /// </summary>
    public partial class UpdateIndividualBuyer : Page
    {
        IndividualBuyer individualbuyer;
        PMS_Main_ModelContainer model=new PMS_Main_ModelContainer();
        public UpdateIndividualBuyer(IndividualBuyer indBuyer)
        {
            InitializeComponent();
            this.individualbuyer = indBuyer;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double d;// to check liability
         
            String error = "";
            individualbuyer = (IndividualBuyer)model.Buyers.Where(i => i.Id == individualbuyer.Id).FirstOrDefault();

            


             Validation v = new Validation();// validation class is used to validate in relevant way
             if (!v.ValidateTextField(txtIName.Text))
                 error = "Enter full name,";
             if (!v.ValidateTextField(txtAddress.Text))
                 error = error + "Enter address,";
             if (!v.ValidateTextField(txtIEmail.Text))
                 error = error + "Enter Email address,";
             if (txtIEmail.Text != "")
             {
                 if (!v.ValidateEmail(txtIEmail.Text))
                     error = error + "Enter valide email,";

             }

             if (txtILiability.Text != "")
             {
                 if (!double.TryParse(txtILiability.Text, out d))
                     error = error + "Enter only numbers for maximum liability";
             }

             if (comboxIStatus.SelectedItem == null)
                 error = error + "select status,";


             if (error == "")
             {
                 individualbuyer.Name = txtIName.Text;
                 individualbuyer.Address = txtAddress.Text;
                 individualbuyer.City = txtICity.Text;
                 individualbuyer.Country.Name = comboIcountry.Text;
                 if (dpIBirthday.SelectedDate != null)
                     individualbuyer.DateOfBirth = dpIBirthday.SelectedDate.Value;
                 individualbuyer.Email = txtIEmail.Text;
                 individualbuyer.Fax = txtIFax.Text;
                 individualbuyer.MaxLiability = txtILiability.Text;


                 individualbuyer.MobileNo = txtIMobileNo.Text;
                 individualbuyer.NIC = txtINic.Text;
                 individualbuyer.Passport = txtIPassport.Text;
                 individualbuyer.TelephoneNo = txtItelphone.Text;
                 individualbuyer.ZipCode = txtIZipCode.Text;
                 individualbuyer.BuyerStatus.description = comboxIStatus.Text;
                 model.SaveChanges();
                 //saving to the activity log
                 //AddActivityLog.AddActivity("Update induvidual buyer");
                 MessageBox.Show("Sucsessfully updated", "Update");
                 ((MainWindow)Application.Current.MainWindow).frame2.Content = null;
                 ((MainWindow)Application.Current.MainWindow).frame1.Content = new SearchIndividualBuyer();
             }
             else
             {
                        MessageBox.Show(error, "Error");
             }


            


        }
        
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            txtIName.Text = individualbuyer.Name;

            txtAddress.Text = individualbuyer.Address;
            txtICity.Text = individualbuyer.City;
            
             dpIBirthday.SelectedDate = individualbuyer.DateOfBirth;
            txtIEmail.Text = individualbuyer.Email;
            txtIFax.Text = individualbuyer.Fax;
            txtILiability.Text = individualbuyer.MaxLiability;
            txtIMobileNo.Text = individualbuyer.MobileNo;
            txtINic.Text = individualbuyer.NIC;
            txtIPassport.Text = individualbuyer.Passport;
            txtItelphone.Text = individualbuyer.TelephoneNo;
            txtIZipCode.Text = individualbuyer.ZipCode;
            BindCountry();
            BindStatus();

            comboIcountry.Text = individualbuyer.Country.Name;
            comboxIStatus.Text = individualbuyer.BuyerStatus.description;
        }

        private void txtIName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }



        private void BindCountry()
        {
            var countries = model.Countries;
            comboIcountry.ItemsSource = countries;
            comboIcountry.DisplayMemberPath = "Name";

            
        }
        private void BindStatus()
        {
            var status = model.BuyerStatus;
            comboxIStatus.ItemsSource = status;
            comboxIStatus.DisplayMemberPath = "description";

        }

    }
}
