﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for UpdateUser.xaml
    /// </summary>
    public partial class UpdateUser : Page
    {
        //Create Entity model oblect
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        User user;
        Validation valid = new Validation();

        string _epfNo;

        public UpdateUser(string epfNo)
        {
            InitializeComponent();
            this._epfNo = epfNo;
             user= model.Users.First(u => u.EpfNo == epfNo);

            txtEPFNoUpdate.Text = user.EpfNo;
            txtFirstNameUpdate.Text = user.FirstName;
            txtLastNameUpdate.Text = user.LastName;
            txtNICUpdate.Text = user.NIC;
            txtDesignationUpdate.Text = user.Designation;
            txtAddressUpdate.Text = user.Address;
            txtMobileNoUpdate.Text = user.MobileNo;
            txtLandPhoneUpdate.Text = user.LandPhone;
            txtEmailUpdate.Text = user.Email;
            txtUserNameUpdate.Text = user.UserName;
            txtPasswordUpdate.Password = user.Password;
            txtRePasswordUpdate.Password = user.Password;
            dtPickerDateOfBirthUpdate.SelectedDate = user.DateOfBirth.Value;

            //Check Gender
            if (user.Gender == "Male")
            {
                chBoxMaleUpdate.IsChecked = true;
            }
            else
            {
                chBoxFemaleUpdate.IsChecked = true;
            }
        }

        //Clear all textboxs
        private void ClearTextBox()
        {
            txtEPFNoUpdate.Text = "";
            txtFirstNameUpdate.Text = "";
            txtLastNameUpdate.Text = "";
            txtNICUpdate.Text = "";
            txtDesignationUpdate.Text = "";
            txtAddressUpdate.Text = "";
            txtMobileNoUpdate.Text = "";
            txtLandPhoneUpdate.Text = "";
            txtEmailUpdate.Text = "";
            txtUserNameUpdate.Text = "";
            txtPasswordUpdate.Password = "";
            txtRePasswordUpdate.Password = "";
            //dtPickerDateOfBirthUpdate.ClearValue(DatePicker.SelectedDateProperty);
        }


        private void btnClearUpdate_Click(object sender, RoutedEventArgs e)
        {
            ClearTextBox();
        }

        private void btnUserUpdate_Click(object sender, RoutedEventArgs e)
        {
            //Update new user

                //Validate requiered fields
            if (valid.ValidateTextField(txtNICUpdate.Text) && valid.ValidateTextField(txtEPFNoUpdate.Text) && valid.ValidateTextField(txtUserNameUpdate.Text) && valid.ValidateTextField(txtPasswordUpdate.Password))
                {
                    //Validate Email
                    if ((valid.ValidateTextField(txtEmailUpdate.Text)) ? valid.ValidateEmail(txtEmailUpdate.Text) : true)
                    {
                        //Validate NIC
                        if (valid.ValidateNIC(txtNICUpdate.Text))
                        {
                            //Validate password
                            if (txtPasswordUpdate.Password.Equals(txtRePasswordUpdate.Password, StringComparison.Ordinal))
                            {
                                //Check paasword length
                                if (txtRePasswordUpdate.Password.Length > 5)
                                {
                                    //Check Gender
                                    if (chBoxMaleUpdate.IsChecked == true)
                                    {
                                        user.Gender = "Male";
                                    }
                                    else
                                    {
                                        user.Gender = "Female";
                                    }

                                    user.EpfNo = txtEPFNoUpdate.Text;
                                    user.FirstName = txtFirstNameUpdate.Text;
                                    user.LastName = txtLastNameUpdate.Text;
                                    user.NIC = txtNICUpdate.Text;
                                    user.Designation = txtDesignationUpdate.Text;
                                    user.Address = txtAddressUpdate.Text;
                                    user.MobileNo = txtMobileNoUpdate.Text;
                                    user.LandPhone = txtLandPhoneUpdate.Text;
                                    user.Email = txtEmailUpdate.Text;
                                    user.DateOfBirth = dtPickerDateOfBirthUpdate.SelectedDate.Value;
                                    user.UserName = txtUserNameUpdate.Text;
                                    user.Password = txtPasswordUpdate.Password;


                                    model.SaveChanges();

                                    ClearTextBox();
                                 
                                    //window.frame2.Content = new ViewUserDetails(this._epfNo);
                                    MainWindow window = (MainWindow)Application.Current.MainWindow;
                                    window.frame2.Content = null;
                                    window.frame1.Content = new ViewUser();

                                    
                                }
                                else
                                {
                                    MessageBox.Show("Passowrds must be more than 6 characters");
                                }
                             }
                            else
                            {
                                MessageBox.Show("Passowrds not match");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid NIC");
                        }
                    }               
                    else
                    {
                        MessageBox.Show("Invalid Email");
                    }
                }
                else
                {
                     MessageBox.Show(" * Fields Required");
                }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                #region
            /*
             
            //Validate username
            IEnumerable<User> users = model.Users.Where(u => u.UserName == txtUserNameUpdate.Text);
            if (true)
            {
                //Validate Epf Number
                IEnumerable<User> usersEpf = model.Users.Where(u => u.EpfNo == txtEPFNoUpdate.Text);
                if (usersEpf.Count() == 0)
                {
                    //Validate requiered fields
                    if (valid.ValidateTextField(txtNICUpdate.Text) && valid.ValidateTextField(txtEPFNoUpdate.Text) && valid.ValidateTextField(txtUserNameUpdate.Text) && valid.ValidateTextField(txtPasswordUpdate.Password))
                    {
                        //Validate Email
                        if ((valid.ValidateTextField(txtEmailUpdate.Text)) ? valid.ValidateEmail(txtEmailUpdate.Text) : true)
                        {
                            //Validate NIC
                            if (valid.ValidateNIC(txtNICUpdate.Text))
                            {
                                //Validate password
                                if (txtPasswordUpdate.Password.Equals(txtRePasswordUpdate.Password, StringComparison.Ordinal))
                                {
                                   
                                }
                                else
                                {
                                    MessageBox.Show("Passowrds not match");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Invalid NIC");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid Email");
                        }
                    }
                    else
                    {
                        MessageBox.Show(" * Fields Required");
                    }
                }
                else
                {
                    MessageBox.Show("Username exist");
                }
            }*/
            #endregion
        }

        private void chBoxFemaleUpdate_Checked(object sender, RoutedEventArgs e)
        {
            chBoxMaleUpdate.IsChecked = false;
        }

        private void chBoxMaleUpdate_Checked(object sender, RoutedEventArgs e)
        {
            chBoxFemaleUpdate.IsChecked = false;
        }

        private void btnDelUser_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to delete?", "Confirm delete",MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                //Delete User
                User delUser = model.Users.Where(i => i.EpfNo == this._epfNo).FirstOrDefault();
                model.Users.DeleteObject(delUser);
                try
                {
                    model.SaveChanges();
                }
                catch
                {
                    MessageBox.Show("Unable To Delete Record");
                }

                MainWindow window = (MainWindow)Application.Current.MainWindow;
                window.frame2.Content = null;
                window.frame1.Content = new ViewUser();
            }
        }
    }
}
