﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace PMS
{
    /// <summary>
    /// Interaction logic for ViewUserDetails.xaml
    /// </summary>
    public partial class ViewUserDetails : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        User user;
        Validation valid = new Validation();

        string _epfNo;

        public ViewUserDetails(string epfNo)
        {
            InitializeComponent();
            this._epfNo = epfNo;
            user = model.Users.First(u => u.EpfNo == epfNo);

            lblEPFNo.Content = user.EpfNo;
            lblFirstName.Content = user.FirstName;
            lblLastName.Content = user.LastName;
            lblNIC.Content = user.NIC;
            lblDesignation.Content = user.Designation;
            lblAddress.Content = user.Address;
            lblMobileNo.Content = user.MobileNo;
            lblLandPhone.Content = user.LandPhone;
            lblEmail.Content = user.Email;
            lblUserName.Content = user.UserName;
            lblDateOfBirth.Content = user.DateOfBirth.Value;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
             
             MainWindow window = (MainWindow)Application.Current.MainWindow;
             window.frame2.Content = new UpdateUser(this._epfNo);
            
        }
    }
}
