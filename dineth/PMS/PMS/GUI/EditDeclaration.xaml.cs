﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for EditDeclaration.xaml
    /// </summary>
    public partial class EditDeclaration : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Declaration declaration;
        public EditDeclaration()
        {
            InitializeComponent();
        }
        public EditDeclaration(int id)
        {
            declaration = (Declaration)model.Declarations.Where(decla => decla.Id == id).First();
            InitializeComponent();
        }

        //Fill Exporter
        private void fillExporter()
        {
            txtExporter.ItemsSource = model.Exporters;
            txtExporter.DisplayMemberPath = "Name";
        }
        //Fill Buyer
        private void fillBuyer()
        {
            txtBuyer.ItemsSource = model.Buyers;
            txtBuyer.DisplayMemberPath = "Name";
        }

        //Fill Policy
        private void fillPolicy()
        {
            txtPolicyNo.ItemsSource = model.Policies;
            txtPolicyNo.DisplayMemberPath = "PolicyNumber";
        }
        //Fill Commodity
        private void fillCommodity()
        {
            txtCommodity.ItemsSource = model.Commodities;
            txtCommodity.DisplayMemberPath = "Name";
        }
        //Fill Country
        private void fillCountry()
        {
            txtCountry.ItemsSource = model.Countries;
            txtCountry.DisplayMemberPath = "Name";
        }
        //fill Details
        private void fillDetails()
        {
            txtExporter.SelectedItem = declaration.Exporter;
            //txtBuyer.SelectedItem = declaration.Buyer;
            txtPolicyNo.SelectedItem = declaration.Policy;
            //txtCommodity.SelectedItem = declaration.Commodity;
            //txtCountry.SelectedItem = declaration.Country;
            //dtPickerShpDate.SelectedDate = declaration.ShipmentDate;
            dtPickerStampDate.SelectedDate = declaration.StampDate;
            //txtGross.Text = declaration.GrossValue;
            //txtCreditDuration.Text = declaration.CreditDuration;
            //txtPaymentTerm.Text = declaration.TermOfPayment;


        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            fillExporter();
            fillBuyer();
            fillPolicy();
            fillCommodity();
            fillCountry();
            fillDetails();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            declaration.Exporter = (Exporter)txtExporter.SelectedItem;
            //declaration.Buyer = (Buyer)txtBuyer.SelectedItem;
            declaration.Policy = (Policy)txtPolicyNo.SelectedItem;
            //declaration.Commodity = (Commodity)txtCommodity.SelectedItem;
            //declaration.Country = (Country)txtCountry.SelectedItem;
            //declaration.ShipmentDate = dtPickerShpDate.SelectedDate.Value;
            declaration.StampDate = dtPickerStampDate.SelectedDate.Value;
            //declaration.GrossValue = txtGross.Text;
            //declaration.CreditDuration = txtCreditDuration.Text;
            

            model.SaveChanges();

            AddActivityLog.AddActivity("Updated Declaration : " +declaration.Id.ToString());

            //(((SearchDeclaration)((MainWindow)Application.Current.MainWindow).frame1.Content)).
            MessageBox.Show("Details Updated Successfully");

        }

        private void txtCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
