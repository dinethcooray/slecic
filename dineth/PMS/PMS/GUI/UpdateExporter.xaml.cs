﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for UpdateExporter.xaml
    /// </summary>
    public partial class UpdateExporter : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Exporter exporter;
        public UpdateExporter()
        {
            InitializeComponent();
            txtStatusUpd.ItemsSource = model.ExporterStatus;
            txtStatusUpd.DisplayMemberPath = "Description";
            txtStatusUpd.SelectedItem = 1;
        }


        public UpdateExporter(int ExpId)
        {
            InitializeComponent();
            //create a exporter object if the exp id is given
           exporter=(Exporter)model.Exporters.Where(exp=>exp.Id==ExpId).First();
           txtNameUpd.Text = exporter.Name;
           txtRegNoUpd.Text = exporter.RegNo;
           txtShortTagUpd.Text=exporter.ShortTag;
           txtTelNoUpd.Text = exporter.TelNo;
           txtEmailUpd.Text = exporter.Email;
           txtDescriptionUpd.Text = exporter.Description;
           txtAddressUpd.Text = exporter.Address;
            

        }


        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Validation validation = new Validation();
            
                //validation for emails
                if (validation.ValidateTextField(txtEmailUpd.Text) ? validation.ValidateEmail(txtEmailUpd.Text) : true)
                {

                    exporter.Name = txtNameUpd.Text;
                    exporter.RegNo = txtRegNoUpd.Text;
                    exporter.TelNo = txtTelNoUpd.Text;
                    exporter.ShortTag = txtShortTagUpd.Text;
                    exporter.Email = txtEmailUpd.Text;
                    exporter.Description = txtDescriptionUpd.Text;
                    exporter.Address = txtAddress.Text;
                    exporter.ExporterStatu = (ExporterStatus)txtStatusUpd.SelectedItem;
                
                    model.SaveChanges();
                    
                }
                else
                {
                    MessageBox.Show("Invalid Email");
                }
            
        }

        private void btnClearUpd_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
