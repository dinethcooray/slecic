﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;


namespace PMS
{
	public partial class AddDeclaration
	{
        
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();

		public AddDeclaration()
		{
			this.InitializeComponent();

			
		}

        private void button1_Click(object sender, RoutedEventArgs e)
        {

        }

         private void button2_Click(object sender, RoutedEventArgs e)
        {
             //checking for access
        //    if (!CheckUserAccess.checkAccess("add_decl")) { MessageBox.Show("You do not have permission to perform the action. Please contact the system Administrator"); return; }

            if (this.Validate())
            {
                Declaration declaration = new Declaration();
                string exporterName = txtExporter.Text;
                Exporter exporter;
                try
                {
                     exporter= model.Exporters.Where(exp => exp.Name == exporterName).FirstOrDefault();
                     declaration.Exporter = exporter;   
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Invalid Exporter Name");
                    return;
                }
                //Buyer buyer;
                //try
                //{
                //    buyer = model.Buyers.Where(buy => buy.Name == txtBuyer.Text).FirstOrDefault();
                //    declaration.Buyer = buyer;
                //}
                //catch(Exception ex)
                //{
                //    MessageBox.Show("Invalid Buyer Name");
                //    return;
                //}
                Policy policy;
                try
                {
                    policy = model.Policies.Where(pol => pol.PolicyNumber == txtPolicyNo.Text).FirstOrDefault();
                    declaration.Policy = policy;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Invalid Policy Number");
                    return;
                }
                //declaration.TermOfPayment = txtPaymentTerm.Text;
                //declaration.Commodity = (Commodity)txtCommodity.SelectedItem;
                //declaration.Country = (Country)txtCountry.SelectedItem;
                //PremiumRate premium;
                //try
                //{
                //    premium = declaration.Country.PremiumRates.Where(prem => prem.CommodityId == declaration.CommodityId).FirstOrDefault();
                //    declaration.PremiumRate = premium;
                //}
                //catch (Exception ex)
                //{
                //}
               
                declaration.StampDate = dtPickerStampDate.SelectedDate.Value;
                declaration.DeclarationStatu = (DeclarationStatus)txtStatus.SelectedItem;
                declaration.Comment = txtComment.Text;

                

                //Adding the declaration to the model
                model.AddToDeclarations(declaration);

                //Saving to the database
                model.SaveChanges();


                ////saving to the activity log
                //AddActivityLog.AddActivity("Added New Declaration :" + declaration.Id.ToString());

                MainWindow window = (MainWindow)Application.Current.MainWindow;
                window.frame2.Content = new AddShipmentsForDeclaration(declaration);
             

            }
            else
            {
                return;
            }
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            
            this.FillExporter();
            this.FillPolicy();
            this.fillStatus();
         
            dtPickerStampDate.SelectedDate = DateTime.Now;
        }

       
        //Fill Exporter
        private void FillExporter()
        {
            txtExporter.ItemsSource = model.Exporters;
            txtExporter.DisplayMemberPath = "Name";
        }
     
        //Fill Policy
        private void FillPolicy()
        {
            txtPolicyNo.ItemsSource = model.Policies;
            txtPolicyNo.DisplayMemberPath = "PolicyNumber";
        }

        //Fill status
        private void fillStatus()
        {
            txtStatus.ItemsSource = model.DeclarationStatus;
            txtStatus.DisplayMemberPath = "Name";
        
        }
        // Validating the Inputs
        private bool Validate()
        {
            return true;
        }

     

        //Clearing the Fields
        private void ClearFields()
        {

            txtExporter.Text = null;
            txtPolicyNo.Text = null;
            txtStatus.Text = null;
            txtComment.Clear();
            dtPickerStampDate.ClearValue(DatePicker.SelectedDateFormatProperty);
            
        }

        //Clear Button click event
        private void button1_Click_1(object sender, RoutedEventArgs e)
        {
            this.ClearFields();
        }

        private void txtStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        
	}
}