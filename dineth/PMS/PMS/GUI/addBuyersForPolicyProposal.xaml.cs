﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PMS
{
    //djhd
	public partial class addBuyersForPolicyProposal
	{
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Policy localPolicy;
        policyShipment policyShip;

        public addBuyersForPolicyProposal(Policy policy)
        {

            policyShip = new policyShipment();
            localPolicy = policy;
            this.InitializeComponent();

            cmbBuyer.ItemsSource = model.Buyers;
            cmbBuyer.DisplayMemberPath = "Name";

            cmbCommodity.ItemsSource = model.Commodities;
            cmbCommodity.DisplayMemberPath = "Name";

            cmbCountry.ItemsSource = model.Countries;
            cmbCountry.DisplayMemberPath = "Name";
        
        
        }
		public addBuyersForPolicyProposal()
		{
			this.InitializeComponent();






			// Insert code required on object creation below this point.
		}

        private void fillTable()
        {
            IEnumerable<policyShipment> policyShipment = model.policyShipments;

        
        }
        private void grdBuyersForPolicies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            policyShip.PolicyId = localPolicy.Id;
            policyShip.Buyer = (Buyer)cmbBuyer.SelectedItem;
            policyShip.Commodity = (Commodity)cmbCommodity.SelectedItem;
            policyShip.Country = (Country)cmbCountry.SelectedItem;
            //localPolicy.policyShipments.
            model.SaveChanges();
            MessageBox.Show("successfully added");

        }

      
	}
}