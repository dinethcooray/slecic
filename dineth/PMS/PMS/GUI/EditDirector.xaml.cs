﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
	public partial class EditDirector
	{
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Director director;
        //public EditDirector()
        //{
        //    this.InitializeComponent();

        //    // Insert code required on object creation below this point.
        //}
        public EditDirector(Director dir)
        {
            director = dir;
            InitializeComponent();
        }

        private void fillDetails()
        {
            txtAddress.Text = director.Address;
            txtEmail.Text = director.Email;
            txtFirstName.Text = director.FirstName;
            txtLastName.Text = director.LastName;
            txtNIC.Text = director.NIC;
            txtPassportNo.Text = director.PassportNo;
            txtTelNo.Text = director.TelNo;
            dtPickerBirthDay.SelectedDate = director.DateOfBirth;
            dtPickerJoinedDate.SelectedDate = director.JoinedDate;
        
        }
      
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Validation validation = new Validation();
           //validating emails
            if (validation.ValidateTextField(txtEmail.Text) ? validation.ValidateEmail(txtEmail.Text) : true)
            {
                //validating nic
                if(validation.ValidateTextField(txtNIC.Text) ? validation.ValidateNIC(txtNIC.Text) :true)
                {
                    //validating birthdate
                    if (dtPickerBirthDay.SelectedDate != null)
                    {
                        //validating joined date
                        if (dtPickerJoinedDate.SelectedDate != null)
                        {
                            //checking whether nicor passport field fill
                            if (validation.ValidateTextField(txtNIC.Text) || validation.ValidateTextField(txtPassportNo.Text))
                            {
                                director = model.Directors.Where(d => d.Id == director.Id).FirstOrDefault();

                                  director.Address=txtAddress.Text;
                                  director.Email =txtEmail.Text;
                                  director.FirstName =txtFirstName.Text;
                                  director.LastName =txtLastName.Text;
                                  director.NIC =txtNIC.Text;
                                  director.PassportNo =txtPassportNo.Text;
                                  director.TelNo =txtTelNo.Text;
                                  director.DateOfBirth =dtPickerBirthDay.SelectedDate;
                                  director.JoinedDate =dtPickerJoinedDate.SelectedDate;
                                  

                                  model.SaveChanges();
                                  MessageBox.Show("Successfully updated the director record");

                                  txtAddress.Clear();
                                  txtEmail.Clear();
                                  txtFirstName.Clear();
                                  txtLastName.Clear();
                                  txtNIC.Clear();
                                  txtPassportNo.Clear();
                                  txtTelNo.Clear();
                                  dtPickerBirthDay.ClearValue(DatePicker.SelectedDateProperty);
                                  dtPickerJoinedDate.ClearValue(DatePicker.SelectedDateProperty);

                                  MainWindow window = (MainWindow)Application.Current.MainWindow;
                                  window.frame2.Content = null;
                                  window.frame1.Content = new SearchDirector();





                            }
                            else
                            {
                                MessageBox.Show("Either NIC or Passport number is required");

                            }
                        }
                        else
                        {
                            MessageBox.Show("Please select a valid joined date");

                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select a valid birthday");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid NIC");

                }

            }
            else
            {
                MessageBox.Show("Email address is invalid");
            }


        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).frame2.Content = new ViewDirector(director);
        }

        private void page_loaded(object sender, RoutedEventArgs e)
        {
            fillDetails();
        }

        private void btn_remove_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to delete this record?", "Confirm delete",MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
           var  dir = model.Directors.Where(d => d.Id == director.Id).FirstOrDefault();

           try
           {
               dir.Exporters.Clear();
               model.Directors.DeleteObject(dir);
               model.SaveChanges();
               MessageBox.Show("Director deleted successfully");
               ((MainWindow)Application.Current.MainWindow).frame1.Content = new SearchDirector();
               ((MainWindow)Application.Current.MainWindow).frame2.Content = null;
           }
           catch {
               MessageBox.Show("unable to delete the director");
           
           }
        }
        }
	}
}