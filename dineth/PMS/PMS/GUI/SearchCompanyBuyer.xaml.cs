﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
    /// <summary>
    /// Interaction logic for SearchCompanyBuyer.xaml
    /// </summary>
    public partial class SearchCompanyBuyer : Page
    {
        PMS_Main_ModelContainer model;
        PMS_Main_ModelContainer model2;
        public SearchCompanyBuyer()
        {
            InitializeComponent();
            model = new PMS_Main_ModelContainer();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            fillGrid();
        }



        public void fillGrid()
        {

            IEnumerable<CompanyBuyer> companybuyer = model.Buyers.OfType<CompanyBuyer>();


            if (txtCName.Text != null)
            {

                companybuyer = companybuyer.Where(i => i.Name.Contains(txtCName.Text));
            }


            if (comboxCCountry.SelectedItem != null)
            {
                int countryId = ((Country)comboxCCountry.SelectedItem).Id;
                companybuyer = companybuyer.Where(i => i.Country.Id == countryId);
            }



            //binding the datasource
            dgSearchCompany.ItemsSource = companybuyer;




        }

        private void BindCountry()
        {
            var countries = model.Countries;
            comboxCCountry.ItemsSource = countries;
            comboxCCountry.DisplayMemberPath = "Name";

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BindCountry();
            fillGrid();
        }

        private void txtCName_TextChanged(object sender, TextChangedEventArgs e)
        {
          

            fillGrid();
        }

        private void comboxCCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // dgSearchCompany.SelectedIndex = -1;

            fillGrid();
        }

        private void dgSearchCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new CompanyBuyerDetails((CompanyBuyer)dgSearchCompany.SelectedItem);
        




        }
    }
}
