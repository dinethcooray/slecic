﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
	public partial class AddShipmentsForDeclaration

	{
         private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Declaration localDeclaration;
        DeclarationShipment declarationship;

		public AddShipmentsForDeclaration()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}

        public AddShipmentsForDeclaration(Declaration declaration)
        {
            
            localDeclaration = model.Declarations.Where(u => u.Id == declaration.Id).FirstOrDefault();
            this.InitializeComponent();

            txtBuyer.ItemsSource = model.Buyers;
            txtBuyer.DisplayMemberPath = "Name";

            txtCommodity.ItemsSource = model.Commodities;
            txtCommodity.DisplayMemberPath = "Name";

            txtCountry.ItemsSource = model.Countries;
            txtCountry.DisplayMemberPath = "Name";

            txtPaymentTerm.ItemsSource = model.TermOfPayments;
            txtPaymentTerm.DisplayMemberPath = "Name";

        }

        private void txtBuyer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

      //clearing fields
        private void clearFields()
        {
            txtCommodity.SelectedItem = null;
            txtCountry.SelectedItem = null;
            txtPaymentTerm.SelectedItem = null;
            dtPickerShpDate.SelectedDate=DateTime.Now;
            txtBuyer.Text = "";
            txtGross.Text = "";
            txtCreditDuration.Text = "";
            

        }

        private void btnAdd_click(object sender, RoutedEventArgs e)
        {
            declarationship = new DeclarationShipment();
            //validating fields
            if (txtCommodity.SelectedItem == null)
            {
                MessageBox.Show("Please select a commodity");
                return;
            }
            if (txtCountry.SelectedItem == null)
            { 
            MessageBox.Show("please select a country");
                return;
            
            }
            if (dtPickerShpDate.SelectedDate == null)
            {
                MessageBox.Show("Please select shipement date");
                return;
            }
            if(txtCreditDuration.Text=="")
            {
            MessageBox.Show("please enter a credit duration");
                return;
            }
            if (txtGross.Text == "")
            {
                MessageBox.Show("Please enter a gross amount");
                return;
            }
            if(txtBuyer.SelectedItem==null)
            {
            MessageBox.Show("Please select a buyer");
                return;
            }
            if(txtPaymentTerm.SelectedItem==null)
            {
            MessageBox.Show("please select a payment term");
                return;
            }


            //saving data to the database
            declarationship.Declaration = localDeclaration;
            declarationship.Buyer=(Buyer)txtBuyer.SelectedItem;
            declarationship.Commodity=(Commodity)txtCommodity.SelectedItem;
            declarationship.Country=(Country)txtCountry.SelectedItem;
            declarationship.GrossValue=Convert.ToInt32(txtGross.Text);
            declarationship.ShipmentDate=(DateTime)dtPickerShpDate.SelectedDate;
            declarationship.CreditDuration=Convert.ToInt32(txtCreditDuration.Text);
            declarationship.TermOfPayment_1= (TermOfPayment)txtPaymentTerm.SelectedItem;
            model.AddToDeclarationShipments(declarationship);
            int rows=model.SaveChanges();

            if(rows==-1){
                MessageBox.Show("Failed to add the record");
                return;
            }
            MessageBox.Show("Successfully added the record");

            clearFields();

        }

        private void txtPaymentTerm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
	}
}