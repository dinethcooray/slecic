﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
	public partial class IndividualBuyerDetails
	{
        IndividualBuyer individualbuyer;
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
		public IndividualBuyerDetails(IndividualBuyer Buyer)
		{
            if (Buyer != null)
            {
                this.InitializeComponent();
                this.individualbuyer = Buyer;
            }
			// Insert code required on object creation below this point.
		}
        public IndividualBuyerDetails(int id)
        {
            // buyer = (Buyer)model.Buyers.Where(buy => buy.Id == id).First();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (individualbuyer != null)
            {
                lblAddress.Content = individualbuyer.Address;
                lblICity.Content = individualbuyer.City;
                lblILastName.Content = individualbuyer.Name;
                lblIliability.Content = individualbuyer.MaxLiability;
                lblIMobileNo.Content = individualbuyer.MobileNo;
                lblIZipCode.Content = individualbuyer.ZipCode;

                lblICountry.Content = individualbuyer.Country.Name;
                lblIFax.Content = individualbuyer.Fax;
                lblItelphone.Content = individualbuyer.TelephoneNo;
                lblIDateOfBirth.Content = individualbuyer.DateOfBirth;

                lblIEmail.Content = individualbuyer.Email;
                lblIPassport.Content = individualbuyer.Passport;
                lblINic.Content = individualbuyer.NIC;
                lblIStatus.Content = individualbuyer.BuyerStatus.description;


            }
            

          
        }

       

        private void btnIUpdate_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new UpdateIndividualBuyer(this.individualbuyer);
        }

        private void btnIDelete_Click(object sender, RoutedEventArgs e)
        {
            individualbuyer = (IndividualBuyer)model.Buyers.Where(i => i.Id == individualbuyer.Id).FirstOrDefault();
            MessageBoxResult result = MessageBox.Show("Do you really want to delete this  individual buyer ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.No)
            {
                return;
            }
            if (result == MessageBoxResult.Yes)
            {
               
                model.Buyers.DeleteObject(individualbuyer);
                try
                {
                    model.SaveChanges();
                    MainWindow window = (MainWindow)Application.Current.MainWindow;
                    window.frame1.Content = new SearchIndividualBuyer();
                    MessageBox.Show("Successfully deleted " +individualbuyer.Name, "Delete", MessageBoxButton.OK);
                 

                }
                catch
                {
                    MessageBox.Show("Unable to delete record","Error",MessageBoxButton.OK,MessageBoxImage.Error);
                }
            }
        }
      
	}
}