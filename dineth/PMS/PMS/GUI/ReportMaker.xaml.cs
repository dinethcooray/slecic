﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using PMS.Reports_pages;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace PMS.GUI
{
    /// <summary>
    /// Interaction logic for ReportMaker.xaml
    /// </summary>
    public partial class ReportMaker : Page
    {

        PMS_Main_ModelContainer model;

        public String cStatus="";
        public String iStatus="";
        public String eStatus = "";
        public ReportMaker()
        {

            InitializeComponent();
            model = new PMS_Main_ModelContainer();
         
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (iStatus != "")
            {
                individualbuyer_reportwindow ind = new individualbuyer_reportwindow(iStatus);
                ind.Show();
            }
            else
                MessageBox.Show("Select the status from list", "Error");

          
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (cStatus != "")
            {
                companybuyerwindow com = new companybuyerwindow(cStatus);
                com.Show();
            }
            else
                MessageBox.Show("Select the status from list", "Error");
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BindBuyerStautus();
            BindExporterStautus();
        }

        private void testt_Click(object sender, RoutedEventArgs e)
        {
            
           //u  frame2.Content = new myreportpage();
          
        }


        //bind buyers
    public void BindBuyerStautus()
        {
            var status = model.BuyerStatus;
        //bind company buyers
            comboxCStatus.ItemsSource = status;
            comboxCStatus.DisplayMemberPath = "description";
        // bind individual buyers
            comboxIStatus.ItemsSource = status;
            comboxIStatus.DisplayMemberPath = "description";

        }

        //Bind exporters
    public void BindExporterStautus()
    {
        var status = model.ExporterStatus;
        comboxEStatus.ItemsSource = status;
        comboxEStatus.DisplayMemberPath = "Description";

     

    }

    private void comboxCStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
     BuyerStatus bs=(BuyerStatus) comboxCStatus.SelectedItem;
     if (bs != null)
     {
         cStatus = bs.description;
         comboxCStatus.Text = bs.description;
     }
    // comboxEStatus.Text = "";
    // comboxIStatus.Text = "";
     
     
    }

    private void comboxIStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        BuyerStatus bs = (BuyerStatus)comboxIStatus.SelectedItem;
        if (bs != null)
        {
            iStatus = bs.description;
            comboxIStatus.Text = bs.description;
        }
        //comboxEStatus.Text = "";
        //comboxCStatus.Text = "";
        

    }

    private void btnExporterStatus_Click(object sender, RoutedEventArgs e)
    {
        if (eStatus != "")
        {
          ExporterStatusReport exporterReport = new ExporterStatusReport(eStatus);
          exporterReport.Show();
        }
        else
            MessageBox.Show("Select the status from list", "Error");
    }

    private void comboxEStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        ExporterStatus es = (ExporterStatus)comboxEStatus.SelectedItem;
        if (es != null)
        {
            eStatus = es.Description;
            comboxEStatus.Text = es.Description;
        }
       // comboxCStatus.Text = "";
       // comboxIStatus.Text = "";
        
    }

    }
}
