﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for ViewDirector.xaml
    /// </summary>
    public partial class ViewDirector : Page
    {
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        private Director director;
        public ViewDirector(Director dir)
        {
            if (dir != null)
            {
                InitializeComponent();
                this.director = dir;
            }
        }

        private void Load_Director_Details()
        {
            if (director!= null)
            {
                lbl_fName.Content = director.FirstName;
                lbl_lName.Content = director.LastName;
                lbl_nic.Content = director.NIC;
                lbl_pass.Content = director.PassportNo;
                lbl_add.Content = director.Address;
                lbl_birthday.Content = director.DateOfBirth;
                lbl_joinedDate.Content = director.JoinedDate;
                lbl_tel.Content = director.TelNo;
                lbl_email.Content = director.Email;
            }
        
        
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).frame2.Content = new EditDirector(director);


        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var dir = model.Directors.Where(i => i.Id == director.Id).FirstOrDefault();
            model.Directors.DeleteObject(dir);
            model.SaveChanges();
        }

        private void page_loaded(object sender, RoutedEventArgs e)
        {
            this.Load_Director_Details();
        }

    }
}
