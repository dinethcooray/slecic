﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for UpdatePastShipment.xaml
    /// </summary>
    public partial class UpdatePastShipment : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        PastShipment pastShipment;

        public UpdatePastShipment()
        {
            InitializeComponent();
        }
        public UpdatePastShipment(PastShipment ps)
        {
            this.InitializeComponent();
            this.pastShipment = ps;
        }

        private void fillExporter()
        {
            cmbExporter.ItemsSource = model.Exporters;
            cmbExporter.DisplayMemberPath = "Name";

        
        }

        private void fillBuyer()
        {
            cmbBuyer.ItemsSource = model.Buyers;
            cmbExporter.DisplayMemberPath = "Name";
        
        }

        private void fillCommodity()
        {
            cmbCommodity.ItemsSource = model.Commodities;
            cmbCommodity.DisplayMemberPath = "Name";
        
        }
        private void fillCountry()
        {
            cmdCountry.ItemsSource = model.Countries;
            cmdCountry.DisplayMemberPath="Name";
        
        
        }
        private void fillValue()
        {
            
        }

        private void fillDetails()
        {
            cmbBuyer.SelectedItem = pastShipment.Buyer;
            cmbCommodity.SelectedItem = pastShipment.Commodity;
            cmbExporter.SelectedItem = pastShipment.Exporter;
            cmdCountry.SelectedItem = pastShipment.Country;
            txt_value.Text = pastShipment.Value.ToString();
            datePickerPastShipment.SelectedDate = pastShipment.Date;
                

        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            pastShipment.Country = (Country)cmdCountry.SelectedItem;
            pastShipment.Commodity = (Commodity)cmbCommodity.SelectedItem;
            pastShipment.Exporter = (Exporter)cmbExporter.SelectedItem;
            pastShipment.Value = Convert.ToDouble(txt_value.Text);
            pastShipment.Buyer = (Buyer)cmbBuyer.SelectedItem;
             pastShipment.Date=(DateTime)datePickerPastShipment.SelectedDate;
             model.SaveChanges();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void page_loaded(object sender, RoutedEventArgs e)
        {

            fillBuyer();
            fillCommodity();
            fillCountry();
            fillExporter();
            fillValue();
            fillExporter();
            fillDetails();

           

        }
    }
}
