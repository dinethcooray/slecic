﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace PMS
{
    /// <summary>
    /// Interaction logic for ViewPastShipment.xaml
    /// </summary>
    public partial class ViewPastShipment : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Validation valid = new Validation();

        public ViewPastShipment()
        {
            InitializeComponent();
            LoadGrid();
        }

        


        //Load data grid from database
        private void LoadGrid()
        {
            grdPastShipment.ItemsSource = model.PastShipments.Where(i=>i.Exporter.Name.Contains(txtExporter.Text));
            
        }

       
        private void grdPass_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new ViewPastShipmentDetails((PastShipment)grdPastShipment.SelectedItem);
        }

        private void txtExporter_TextChanged(object sender, TextChangedEventArgs e)
        {
            LoadGrid();

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            LoadGrid();
        }
    }
}
