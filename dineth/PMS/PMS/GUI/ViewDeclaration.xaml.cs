﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
	public partial class ViewDeclaration
	{
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        private Declaration declaration;
		public ViewDeclaration(Declaration declaration)
		{
			this.InitializeComponent();
            this.declaration = declaration;
            fillGrid();
			// Insert code required on object creation below this point.
		}
        

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.Load_Declaration_Details();

        }

        private void Load_Declaration_Details()
        {
            if (declaration != null)
            {
                lbl_exporter.Content = declaration.Exporter.Name;
                lbl_policy.Content = declaration.Policy.PolicyNumber;
                lbl_stamp_date.Content = declaration.StampDate.ToShortDateString();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //checking for access
         //   if (!CheckUserAccess.checkAccess("edit_decl")) { MessageBox.Show("You do not have permission to perform the action. Please contact the system Administrator"); return; }

            ((MainWindow)Application.Current.MainWindow).frame2.Content = new GUI.UpdateDeclaration(declaration);
        }

        private void fillGrid()
        {
            if (declaration != null)
            {
                IEnumerable<DeclarationShipment> shipments = model.DeclarationShipments;
                shipments=shipments.Where(u=>u.DeclarationId.Equals(declaration.Id));
                grdDeclarationShipment.ItemsSource = shipments;
                
            }
        
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ////checking for access
            //if (!CheckUserAccess.checkAccess("del_decl")) { MessageBox.Show("You do not have permission to perform the action. Please contact the system Administrator"); return; }

            var dec = model.Declarations.Where(i => i.Id == declaration.Id).FirstOrDefault();
            model.Declarations.DeleteObject(dec);
            model.SaveChanges();

            MessageBox.Show("Successfully deleted the record");

            //saving to activity log
            AddActivityLog.AddActivity("Deleted Declaration : " + dec.Id);

            
            ((MainWindow)Application.Current.MainWindow).frame1.Content = new SearchDeclaration();
           
        }

        private void grdPolicyBuyer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
	}
}