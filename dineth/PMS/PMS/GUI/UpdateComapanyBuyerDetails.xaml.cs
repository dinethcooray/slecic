﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for UpdateComapanyBuyerDetails.xaml
    /// </summary>
    public partial class UpdateComapanyBuyerDetails : Page    
    {
        CompanyBuyer companybuyer;
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
    
        public UpdateComapanyBuyerDetails(CompanyBuyer comBuyer)
        {
            InitializeComponent();
            this.companybuyer = comBuyer;
        }

        private void btnCUpdate_Click(object sender, RoutedEventArgs e)
        {
            double d;
        
            String error = "";
            companybuyer = (CompanyBuyer)model.Buyers.Where(i => i.Id == companybuyer.Id).FirstOrDefault();

           

            Validation v = new Validation();



            if (!v.ValidateTextField(txtCName.Text))
                error = "Enter Company name, ";

            if (!v.ValidateEmail(txtCEmail.Text))
                error = error + "Enter valide email, ";

            if (!v.ValidateTextField(txtCAddress.Text))
                error = error + "Enter address, ";

            if (!v.ValidateTextField(txtCRegNo.Text))
                error = error + "Enter Registered no, ";

            if (comboxCCountry.SelectedItem == null)
                error = error + "Select the country,";

            if (comboxCStatus.SelectedItem == null)
                error = error + "Select the status,";

            if(txtCMaxLiability.Text!="")

            if (!double.TryParse(txtCMaxLiability.Text, out d))
                error = error + "Enter only numbers for Maximum liability,";


            if (error == "")
            {


                companybuyer.Address = txtCAddress.Text;
                companybuyer.City = txtCCity.Text;


                companybuyer.Country = (Country)comboxCCountry.SelectedItem;
                companybuyer.Email = txtCEmail.Text;
                companybuyer.Fax = txtCFax.Text;
                companybuyer.Name = txtCName.Text;
                companybuyer.TelephoneNo = txtCTelephoneNo.Text;
                companybuyer.ZipCode = txtCZipcode.Text;
                companybuyer.ShortName = txtShortName.Text;
                companybuyer.RegNo = txtCRegNo.Text;
                companybuyer.MaxLiability = txtCMaxLiability.Text;
                companybuyer.BuyerStatus = (BuyerStatus)comboxCStatus.SelectedItem;
                model.SaveChanges();
                //saving to the activity log
                //AddActivityLog.AddActivity("Update company buyer");
                MessageBox.Show("Update sucessfully", "Update");

                ((MainWindow)Application.Current.MainWindow).frame2.Content = null;
                ((MainWindow)Application.Current.MainWindow).frame1.Content = new SearchCompanyBuyer();
                
            }

            else
            {
                MessageBox.Show(error, "Error");
            }


        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            txtCAddress.Text = companybuyer.Address;
            txtCCity.Text = companybuyer.City;
                    BindCountry();
            txtCEmail.Text = companybuyer.Email;
            txtCFax.Text= companybuyer.Fax;
            txtCName.Text = companybuyer.Name;
            txtCTelephoneNo.Text = companybuyer.TelephoneNo;
            txtCZipcode.Text = companybuyer.ZipCode;
            txtShortName.Text = companybuyer.ShortName;
            txtCRegNo.Text= companybuyer.RegNo;
            txtCMaxLiability.Text = companybuyer.MaxLiability;

            BindCountry();
            BindStatus();
            
         
            comboxCCountry.Text = companybuyer.Country.Name;
            comboxCStatus.Text = companybuyer.BuyerStatus.description;
      
        }

        private void BindCountry()
        {
            var countries = model.Countries;
            comboxCCountry.ItemsSource = countries;
            comboxCCountry.DisplayMemberPath = "Name";
          
           
            
        }


        private void BindStatus()
        {
            var status = model.BuyerStatus;
            comboxCStatus.ItemsSource = status;
            comboxCStatus.DisplayMemberPath = "description";

        }

     
    }
}
