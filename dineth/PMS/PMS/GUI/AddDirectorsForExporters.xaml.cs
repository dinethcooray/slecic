﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{

	public partial class AddDirectorsForExporters
	{
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Exporter exporter;
        Director dir;
		public AddDirectorsForExporters()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}

        public AddDirectorsForExporters(Exporter exp)
        {
            this.exporter = exp;
            this.InitializeComponent();
        
        }

        //listner to fill grid view dynamically
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            this.fillGridView();
        }

        //method to fill grid view
        private void fillGridView()
        {
            IEnumerable<Director> directors = exporter.Directors;
            directors = directors.Where(i => i.FirstName.Contains(txtName.Text));
            gridDirectors.ItemsSource = directors;      
        }

        private void gridDirectors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
            
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void button1_Click_Add(object sender, RoutedEventArgs e)
        {
            dir = (Director)gridDirectors.SelectedItem;
            dir = model.Directors.Where(i => i.Id == dir.Id).FirstOrDefault();
            exporter = model.Exporters.Where(i => i.Id == exporter.Id).FirstOrDefault();
            exporter.Directors.Add(dir);
            model.SaveChanges();
            MessageBox.Show("director successfully added");
        }

        private void button1_Click_close(object sender, RoutedEventArgs e)
        {

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            gridDirectors.ItemsSource = model.Directors;
        }
	}
}