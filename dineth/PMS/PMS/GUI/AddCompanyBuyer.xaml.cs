﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;



namespace PMS
{
	public partial class AddCompanyBuyer
	{
        PMS_Main_ModelContainer model;
		public AddCompanyBuyer()
		{
			this.InitializeComponent();
            model = new PMS_Main_ModelContainer();
			// Insert code required on object creation below this point.
   
		}

        private void btnCAdd_Click(object sender, RoutedEventArgs e)
        {
            double d;
          
            String error = "";
            CompanyBuyer companybuyer = new CompanyBuyer();
        
            
            Validation v = new Validation();
            
         
         

            if (!v.ValidateTextField(txtCName.Text))
                error = "Enter Company name, ";

            if (!v.ValidateEmail(txtCEmail.Text))
                error = error + "Enter valide email, ";

            if(!v.ValidateTextField(txtCAddress.Text))
                error=error+"Enter address, ";

            if (!v.ValidateTextField(txtCRegNo.Text))
                error = error + "Enter Registered no, ";

            if (comboBoxCCountry.SelectedItem == null)
                error = error + "Select the country,";

            if (comboxCStatus.SelectedItem == null)
                error = error + "Select the status,";

            if (txtCMaxLiability.Text != "")
            {
                if (!double.TryParse(txtCMaxLiability.Text, out d))
                    error = error + "Enter only numbers for maximum liability";
            }

           

            if (error=="")
            {
                companybuyer.Country = (Country)comboBoxCCountry.SelectedItem;

                companybuyer.City = txtCCity.Text;
                companybuyer.Address = txtCAddress.Text;
                companybuyer.ZipCode = txtCZipCode.Text;
                companybuyer.Name = txtCName.Text;
                companybuyer.Fax = txtCFax.Text;
                companybuyer.Email = txtCEmail.Text;
                companybuyer.TelephoneNo = txtCTelephoneNo.Text;
                companybuyer.ShortName = txtCShortTag.Text;
                companybuyer.RegNo = txtCRegNo.Text;
                companybuyer.MaxLiability = txtCMaxLiability.Text;
                companybuyer.BuyerStatus = (BuyerStatus)comboxCStatus.SelectedItem;
          
                model.AddToBuyers(companybuyer);
                model.SaveChanges();
                //saving to the activity log
                //AddActivityLog.AddActivity("Add company buyer");
                MessageBox.Show("Successfully added to database", "Added the company");
            }
            else
            {
                MessageBox.Show(error, "Error");
             
            }


         

        }
        private void BindCountry()
        {
            var countries = model.Countries;
            comboBoxCCountry.ItemsSource = countries;
            comboBoxCCountry.DisplayMemberPath = "Name";
           
        }


        private void BindStatus()
        {
            var status = model.BuyerStatus; 
            comboxCStatus.ItemsSource = status;
            comboxCStatus.DisplayMemberPath = "description";

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BindCountry();
            BindStatus();

          
        }

        private void btnCClear_Click(object sender, RoutedEventArgs e)
        {
            comboBoxCCountry.SelectedItem=null;
            txtCCity.Text="";
            txtCAddress.Text="";
               txtCZipCode.Text="";
            txtCName.Text="";
            txtCFax.Text="";
             txtCEmail.Text="";
             txtCTelephoneNo.Text="";
            txtCShortTag.Text="";
             txtCRegNo.Text="";
            txtCMaxLiability.Text="";
            comboxCStatus.SelectedItem=null;
            

        }

       
       
	}
}