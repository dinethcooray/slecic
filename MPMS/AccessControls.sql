/*
Navicat SQL Server Data Transfer

Source Server         : MSSQL
Source Server Version : 100000
Source Host           : .\SQLEXPRESS:1433
Source Database       : PMSDB
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 100000
File Encoding         : 65001

Date: 2012-09-20 22:29:43
*/


-- ----------------------------
-- Table structure for [dbo].[AccessControls]
-- ----------------------------
DROP TABLE [dbo].[AccessControls]
GO
CREATE TABLE [dbo].[AccessControls] (
[Id] int NOT NULL IDENTITY(1,1) ,
[ActionDescription] nvarchar(MAX) NOT NULL ,
[ShortTag] nvarchar(MAX) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[AccessControls]', RESEED, 34)
GO

-- ----------------------------
-- Records of AccessControls
-- ----------------------------
SET IDENTITY_INSERT [dbo].[AccessControls] ON
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'1', N'Login', N'L');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'2', N'Add User', N'Addusr');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'3', N'Assign user to group', N'AsnUsr');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'4', N'Add access group', N'AdAcGr');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'5', N'Add access to group', N'AATG');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'6', N'View activity  log', N'VAL');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'7', N'Add policy proposal', N'APP');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'9', N'Update policy proposal', N'UPP');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'10', N'Remove policy proposal', N'RemPP');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'11', N'Add buyers for policy proposal', N'ABFPP');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'13', N'Update Declaration', N'Updeca');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'14', N'Remove declaration', N'RemDecl');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'15', N'Add Declaration', N'AdDecl');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'16', N'Add shipments for the declaration', N'Addshp');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'17', N'Exporter edit', N'EE');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'18', N'Ecporter delete', N'ExpD');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'19', N'Add drector', N'ADDD');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'20', N'update director', N'UPD');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'21', N'Remove director', N'RemD');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'22', N'Add past shipment', N'APS');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'23', N'Individual buyer update', N'IBUpd');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'24', N'individual buyer Add', N'IBA');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'25', N'individual buyer remove', N'IBR');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'26', N'Company buyer update', N'CBU');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'27', N'Company buyer add', N'CBA');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'28', N'Company buyer remove', N'CBR');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'32', N'Add country rates', N'ACR');
GO
INSERT INTO [dbo].[AccessControls] ([Id], [ActionDescription], [ShortTag]) VALUES (N'34', N'Send email', N'SE');
GO
SET IDENTITY_INSERT [dbo].[AccessControls] OFF
GO

-- ----------------------------
-- Indexes structure for table AccessControls
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table [dbo].[AccessControls]
-- ----------------------------
ALTER TABLE [dbo].[AccessControls] ADD PRIMARY KEY ([Id])
GO
