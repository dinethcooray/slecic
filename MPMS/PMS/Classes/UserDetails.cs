﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PMS
{
    class UserDetails
    {
        /// <summary>
        /// Static value protected by access routine.
        /// </summary>
        static int _userId;
        static string _userName;

        /// <summary>
        /// Access routine for global variable.
        /// </summary>
        public static int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public static string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

    }
}
