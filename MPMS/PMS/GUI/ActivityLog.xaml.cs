﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Collections;

namespace PMS
{
    /// <summary>
    /// Interaction logic for ActivityLog.xaml
    /// </summary>
    public partial class ActivityLog : Page
    {
        public ActivityLog()
        {
            InitializeComponent();
        }

        private void btnActivityView_Click(object sender, RoutedEventArgs e)
        {
            //Load activity log according to date
            viewLog(datePickerLog.SelectedDate.Value.ToString("M-dd-yyyy"));
        }

        public void viewLog(string day)
        {

            string path = @"./Activity/PMS_" + day + ".txt";
            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path);

                ArrayList aData = new ArrayList();

                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string myString = line;
                    char[] separator = new char[] { '\t' };
                    string[] activitys = myString.Split(separator);

                    object[] Log = new object[] { "", "", "" };
                    if (activitys.Length > 2)
                    {
                        //---Object initializer
                        aData.Add(new FileData()
                        {
                            Action = activitys[0].ToString(),
                            EPF = activitys[1].ToString(),
                            User = activitys[2].ToString()
                        });
                    }


                }
                grdActivityLog.ItemsSource = aData;
            }
            else
            {
                MessageBox.Show("File not exists");
            }
        }

        private void txtSearchKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            string path = @"./Activity/PMS_" + datePickerLog.SelectedDate.Value.ToString("M-dd-yyyy") + ".txt";
            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path);

                ArrayList aData = new ArrayList();

                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Contains(txtSearchKey.Text))
                    {
                        string myString = line;
                        char[] separator = new char[] { '\t' };
                        string[] activitys = myString.Split(separator);

                        object[] Log = new object[] { "", "", "" };
                        if (activitys.Length > 2)
                        {
                            //---Object initializer
                            aData.Add(new FileData()
                            {
                                Action = activitys[0].ToString(),
                                EPF = activitys[1].ToString(),
                                User = activitys[2].ToString()
                            });
                        }
                    }


                }
                grdActivityLog.ItemsSource = aData;
            }
            else
            {
                MessageBox.Show("File not exists");
            }
         
        }

    }
}
