﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
	public partial class UpdateDeclarationShipments
	{
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        private DeclarationShipment shipment;
		public UpdateDeclarationShipments()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}
        public UpdateDeclarationShipments(DeclarationShipment ship)
        {
            this.InitializeComponent();
            shipment = model.DeclarationShipments.Where(u => u.Id == ship.Id).FirstOrDefault();
            fillComboBoxes();
            loadDetails();


   
        }

        private void fillComboBoxes()
        {
            txtBuyer.ItemsSource = model.Buyers;
            txtBuyer.DisplayMemberPath = "Name";

            txtCommodity.ItemsSource = model.Commodities;
            txtCommodity.DisplayMemberPath = "Name";

            txtCountry.ItemsSource = model.Countries;
            txtCountry.DisplayMemberPath = "Name";
        
        }

        private void loadDetails()
        {
            txtBuyer.SelectedItem = shipment.Buyer;
            txtCommodity.SelectedItem = shipment.Commodity;
            txtCountry.SelectedItem = shipment.Country;
            txtPaymentTerm.SelectedValue = shipment.TermOfPayment;
            dtPickerShpDate.SelectedDate = shipment.ShipmentDate;
            txtGross.Text = shipment.GrossValue.ToString();
            txtCreditDuration.Text = shipment.CreditDuration.ToString();
       
        
        }


        private void btnAdd_click(object sender, RoutedEventArgs e)
        {

        }

        private void txtPaymentTerm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnSave_click(object sender, RoutedEventArgs e)
        {
            
            if (txtPaymentTerm.SelectedItem == null)
            {
                MessageBox.Show("please select a term of payment");
                return;
            
            }
            shipment.Buyer=(Buyer)txtBuyer.SelectedItem;
            shipment.Commodity = (Commodity)txtCommodity.SelectedItem;
            shipment.Country = (Country)txtCountry.SelectedItem;
            shipment.TermOfPayment = txtPaymentTerm.SelectedValue.ToString();
            shipment.ShipmentDate = (DateTime)dtPickerShpDate.SelectedDate;
            shipment.GrossValue = Convert.ToInt32(txtGross.Text);
            shipment.CreditDuration = Convert.ToInt32(txtCreditDuration.Text);

            int rows=model.SaveChanges();
            if (rows == -1)
            {
                MessageBox.Show("Unable to save changes");
                return;
            }
            MessageBox.Show("Successfully saved changes");


        }

        private void btnCancel_click(object sender, RoutedEventArgs e)
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new GUI.UpdateDeclaration((Declaration)shipment.Declaration);
        }
	}
}