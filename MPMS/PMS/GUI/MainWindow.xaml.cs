﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Windows.Controls.Ribbon;
using PMS.GUI;
using System.ComponentModel;

namespace PMS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        Logging lg;
        public MainWindow()
        {
            InitializeComponent();
            if (UserDetails.UserId == 0)
            {
                lg = new Logging(this);
                this.Hide();
                lg.Show();
            }

        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (UserDetails.UserId > 0)
            {
                if (MessageBox.Show("Do you really want to Log Out?", "Log Out Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    lg = new Logging(this);
                    this.Hide();
                    lg.Show();
                }
                 
                base.OnClosing(e);
                e.Cancel = true;
            }
        }

        public void CheckLogUserAccess()
        {
            //checking for access User Panel
            if (CheckUserAccess.checkAccess("MUSER") || CheckUserAccess.checkAccess("MACC"))
            {
                USER_TAB.Visibility = Visibility.Visible;
                if (!CheckUserAccess.checkAccess("MUSER"))
                {
                    USER_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    USER_PANEL.Visibility = Visibility.Visible;
                }

                if (!CheckUserAccess.checkAccess("MACC"))
                {
                    ACCESS_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ACCESS_PANEL.Visibility = Visibility.Visible;
                }
            }
            else
            {
                USER_TAB.Visibility = Visibility.Collapsed;
            }

            //checking for access Policy Panel
            if (CheckUserAccess.checkAccess("MPP") || CheckUserAccess.checkAccess("MDEC"))
            {
                POLICY_TAB.Visibility = Visibility.Visible;
                if (!CheckUserAccess.checkAccess("MPP"))
                {
                    POLICY_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    POLICY_PANEL.Visibility = Visibility.Visible;
                }

                if (!CheckUserAccess.checkAccess("MDEC"))
                {
                    DECLARATION_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    DECLARATION_PANEL.Visibility = Visibility.Visible;
                }

            }
             else
            {
                 POLICY_TAB.Visibility = Visibility.Collapsed;
            }

            //checking for access Exporter Panel
            if (CheckUserAccess.checkAccess("MEXP") || CheckUserAccess.checkAccess("MDIR") || CheckUserAccess.checkAccess("MSHIP"))
            {
                EXPORTER_TAB.Visibility = Visibility.Visible;
                if (!CheckUserAccess.checkAccess("MEXP"))
                {
                    EXPORTER_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    EXPORTER_PANEL.Visibility = Visibility.Visible;
                }

                if (!CheckUserAccess.checkAccess("MDIR"))
                {
                    DIRECTER_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    DIRECTER_PANEL.Visibility = Visibility.Visible;
                }

                if (!CheckUserAccess.checkAccess("MSHIP"))
                {
                    SHIPMENT_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    SHIPMENT_PANEL.Visibility = Visibility.Visible;
                }

            }
            else
            {
                EXPORTER_TAB.Visibility = Visibility.Collapsed;
            }

            //checking for access Buyer Panel
            if (CheckUserAccess.checkAccess("MIBUYER") || CheckUserAccess.checkAccess("MCBUYER"))
            {
                BUYER_TAB.Visibility = Visibility.Visible;
                if (!CheckUserAccess.checkAccess("MIBUYER"))
                {
                    IBUYER_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    IBUYER_PANEL.Visibility = Visibility.Visible;
                }

                if (!CheckUserAccess.checkAccess("MCBUYER"))
                {
                    CBUYER_PANEL.Visibility = Visibility.Collapsed;
                }
                else
                {
                    CBUYER_PANEL.Visibility = Visibility.Visible;
                }

            }
            else
            {
                BUYER_TAB.Visibility = Visibility.Collapsed;
            }

            //checking for access Premium Panel
            if (!CheckUserAccess.checkAccess("MPRM"))
            {
                PREMIUM_TAB.Visibility = Visibility.Collapsed;
            }
            else
            {
                PREMIUM_TAB.Visibility = Visibility.Visible;
            }

            //checking for access Email and Reports Panel
            if (!CheckUserAccess.checkAccess("MRNE"))
            {
                EMAIL_TAB.Visibility = Visibility.Collapsed;
            }
            else
            {
                EMAIL_TAB.Visibility = Visibility.Visible;
            }
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new SearchDeclaration();
            frame2.Content = null;
         
        }

        private void LayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //frame1.SetValue(WidthProperty, frame1.GetValue(Grid.WidthProperty));
        }

        private void RibbonButton_Click_4(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new ViewUser();
            frame2.Content = null;
        }

        private void RibbonButton_Click_5(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new AddUser();
            frame2.Content = null;
        }

        private void RibbonButton_Click_6(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new AddAccessToUser();
            frame2.Content = null;
        }

        private void btnAddGroup_Click(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            this.CheckLogUserAccess();
            frame1.Content = new AddAccessGroup();
            frame2.Content = null;
        }

        private void btnAssignAccess_Click(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new AddAccessToGroup();
            frame2.Content = null;
        }

        private void RibbonButton_Click(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new SearchPolicyProposal();
            frame2.Content = null;
        }

        private void RibbonButton_Click_14(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new AddPolicyProposal();
            frame2.Content = null;
        }

        private void RibbonButton_Click_12(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new SearchDeclaration();
            frame2.Content = null;
        }

        private void RibbonButton_Click_13(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new AddDeclaration();
            frame2.Content = null;
        }

        private void RibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            //search exporter
            this.CheckLogUserAccess();
            frame1.Content = new SearchExporters();
            frame2.Content = null;
        }

        private void RibbonButton_Click_2(object sender, RoutedEventArgs e)
        {
            //add exporter
            this.CheckLogUserAccess();
            frame1.Content = new AddExporter();
            frame2.Content = null;
        }

        private void RibbonButton_Click_3(object sender, RoutedEventArgs e)
        {
            //search director
            this.CheckLogUserAccess();
            frame1.Content = new SearchDirector();
            frame2.Content = null;
        }

        private void RibbonButton_Click_7(object sender, RoutedEventArgs e)
        {
            //add director
            this.CheckLogUserAccess();
            frame1.Content = new AddDirector();
            frame2.Content = null;
        }

        private void RibbonButton_Click_8(object sender, RoutedEventArgs e)
        {
            //search shipment
            this.CheckLogUserAccess();
            frame1.Content = new ViewPastShipment();
            frame2.Content = null;
        }

        private void RibbonButton_Click_9(object sender, RoutedEventArgs e)
        {
            //add shipment
            this.CheckLogUserAccess();
            frame1.Content = new AddPastShipmentDetails();
            frame2.Content = null;
        }

        private void RibbonButton_Click_10(object sender, RoutedEventArgs e)
        {
            //search ind buyer
            this.CheckLogUserAccess();
            frame1.Content = new SearchIndividualBuyer();
            frame2.Content = null;
        }

        private void RibbonButton_Click_11(object sender, RoutedEventArgs e)
        {
            //add ind buyer
            this.CheckLogUserAccess();
            frame1.Content = new AddIndividualBuyer();
            frame2.Content = null;
        }

        private void RibbonButton_Click_15(object sender, RoutedEventArgs e)
        {
            //search com buyer
            this.CheckLogUserAccess();
            frame1.Content = new SearchCompanyBuyer();
            frame2.Content = null;
        }

        private void RibbonButton_Click_16(object sender, RoutedEventArgs e)
        {
            //add com buyer
            this.CheckLogUserAccess();
            frame1.Content = new AddCompanyBuyer();
            frame2.Content = null;
        }

        private void RibbonButton_Click_17(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new AddPremiumRates();
            frame2.Content = null;
        }

        private void btnAddGroup_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new ActivityLog();
            frame2.Content = null;
        }

        private void RibbonButton_Click_Email_Sender(object sender, RoutedEventArgs e)
        {
            this.CheckLogUserAccess();
            frame1.Content = new EmailSender();
            frame2.Content = null;
        }
        
    }
}
