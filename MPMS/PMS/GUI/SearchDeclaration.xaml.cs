﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;
using System.Reflection;

namespace PMS
{
    public partial class SearchDeclaration
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
       
        public SearchDeclaration()
        {
            this.InitializeComponent();

            // Insert code required on object creation below this point.
        }
       

        //Filling the datagrid with the declaration Data
        public void fillGrid()
        {
            
            
            IEnumerable<Declaration> declarations = model.Declarations;
            //exporter
            
            if (txt_exporter.SelectedItem != null)
            {
                int exporterId = ((Exporter)txt_exporter.SelectedItem).Id;
                 declarations = declarations.Where(i => i.Exporter.Id == exporterId);
            }
            
            
            //policy 
            if (txt_policy.SelectedItem != null)
            {
                int policyId = ((Policy)txt_policy.SelectedItem).Id;
                declarations = declarations.Where(i => i.Policy.Id == policyId);
            }

          
            
            //binding the datasource
            grdDeclarations.ItemsSource = declarations;
            

    
            
        }

        //Filling the exporter
        private void fillExporter()
        {
            var exporters = model.Exporters;
            txt_exporter.ItemsSource = exporters;
            
            txt_exporter.DisplayMemberPath = "Name";
            
            
        }

      
        
        //Filling the Policy
        private void fillPolicy()
        {

            var policies = model.Policies;
            txt_policy.ItemsSource = policies;
            txt_policy.DisplayMemberPath ="PolicyNumber";
        }

       
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            fillExporter();
          
            fillPolicy();
      
            fillGrid();
         
        }

        private void txt_exporter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            this.fillGrid();
          
           
        }

        private void txtBuyer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            this.fillGrid();
        }

        private void txt_policy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
            this.fillGrid();
        }

        private void txtCommodity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            this.fillGrid();
        }

        private void grdDeclarations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
            var selectedDeclaration = grdDeclarations.SelectedItem;
            
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new ViewDeclaration((Declaration)selectedDeclaration);
            


        }

        
 
    }

}