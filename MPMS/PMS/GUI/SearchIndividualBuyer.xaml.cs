﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for SearchIndividualBuyer.xaml
    /// </summary>
    public partial class SearchIndividualBuyer : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        public SearchIndividualBuyer()
        {
            InitializeComponent();
        }

        private void btnSearchForIndividualBuyers_Click(object sender, RoutedEventArgs e)
        {

        }

        public void fillGrid()
        {
            IEnumerable<IndividualBuyer> individualbuyer = model.Buyers.OfType<IndividualBuyer>();


            if (txtIFName.Text != null)
            {

                individualbuyer = individualbuyer.Where(i => i.Name.Contains(txtIFName.Text));
            }
            if (txtICity.Text != null)
            {

                individualbuyer = individualbuyer.Where(i => i.Name.Contains(txtICity.Text));
            }



            if (combIcountry.SelectedItem != null)
            {

                individualbuyer = individualbuyer.Where(i => i.Country == combIcountry.SelectedItem);
            }



            //binding the datasource
            dgSearchForIndividuaBuyer.ItemsSource = individualbuyer;




        }

        private void BindCountry()
        {
            var countries = model.Countries;
            combIcountry.ItemsSource = countries;
            combIcountry.DisplayMemberPath = "Name";
     
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BindCountry();
            fillGrid();
        }

        private void txtIFName_TextChanged(object sender, TextChangedEventArgs e)
        {
            fillGrid();
        }

      

        private void combIcountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            fillGrid();
        }

        private void dgSearchForIndividuaBuyer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new IndividualBuyerDetails((IndividualBuyer)dgSearchForIndividuaBuyer.SelectedItem);
        }

        private void txtICity_TextChanged(object sender, TextChangedEventArgs e)
        {
            fillGrid();
        }

    }
}
