﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for AddPolicyProposal.xaml
    /// </summary>
    public partial class AddPolicyProposal : Page
    {
        private bool validate = false;
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        public AddPolicyProposal()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
           
            FillExporter();

        }

        //FILL Exporter
        private void FillExporter()
        {
            cmb_exporter.ItemsSource = model.Exporters;
            cmb_exporter.DisplayMemberPath = "Name";
        }

        //checking whether the policy number is available 
        private bool validatePolicyNumber()
        {
            if (model.Policies.Where(i => i.PolicyNumber == txt_polNo.Text).Count() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //validating fields
            if (!validatePolicyNumber())
            {
                MessageBox.Show("Policy Number Already Exists");
                return ;
            }
            if (cmb_exporter.SelectedItem == null)
            {
                MessageBox.Show("Please Select Exporter");
                return;
            }
            if (txt_duration.Text == "")
            {
                MessageBox.Show("Please Enter Duration");
                return;
            }
            if (txt_value.Text == "")
            {
                MessageBox.Show("Please Enter Value");
                return;
            }

            //saving to the data base
            Policy policy = new Policy();
            policy.PolicyNumber = txt_polNo.Text;
            policy.Value = Convert.ToDouble(txt_value.Text);
            policy.Duration = Convert.ToInt32(txt_duration.Text);
            policy.Exporter = (Exporter)cmb_exporter.SelectedItem;
            policy.RecievedDate = DateTime.Now;
            policy.StartDate = dtp_start_date.SelectedDate;
           

            
            model.SaveChanges();

            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new addBuyersForPolicyProposal(policy);
      

            
        }

        private void cmb_buyer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
