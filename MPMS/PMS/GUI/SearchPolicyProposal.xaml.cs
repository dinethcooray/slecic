﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS.GUI
{
    /// <summary>
    /// Interaction logic for SearchPolicyProposal.xaml
    /// </summary>
    public partial class SearchPolicyProposal : Page
    {
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        public SearchPolicyProposal()
        {
            InitializeComponent();
            FillTable();
        }

        private void txt_exporter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void FillTable()
        {
            IEnumerable<Policy> policies = model.Policies;
            policies = policies.Where(i => i.PolicyNumber.Contains(txt_polNo.Text));
            grdPolicies.ItemsSource = policies;

            
        }

        private void txt_polNo_TextChanged(object sender, TextChangedEventArgs e)
        {
            FillTable();
        }

        private void grdPolicies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new UpdatePolicyProposal(((Policy)grdPolicies.SelectedItem));
        }
    }
}
