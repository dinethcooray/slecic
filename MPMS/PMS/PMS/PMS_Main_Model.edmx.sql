
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 09/19/2012 10:39:57
-- Generated from EDMX file: C:\Documents and Settings\Administrator\Desktop\MPMS_2012_09_18\PMS\PMS\PMS\PMS\PMS_Main_Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [PMSDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__AccessCon__Acces__28ED12D1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AccessControlAccessGroup] DROP CONSTRAINT [FK__AccessCon__Acces__28ED12D1];
GO
IF OBJECT_ID(N'[dbo].[FK__AccessCon__Acces__29E1370A]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AccessControlAccessGroup] DROP CONSTRAINT [FK__AccessCon__Acces__29E1370A];
GO
IF OBJECT_ID(N'[dbo].[FK__AccessGro__Acces__2AD55B43]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AccessGroupUser] DROP CONSTRAINT [FK__AccessGro__Acces__2AD55B43];
GO
IF OBJECT_ID(N'[dbo].[FK__AccessGro__Users__2BC97F7C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AccessGroupUser] DROP CONSTRAINT [FK__AccessGro__Users__2BC97F7C];
GO
IF OBJECT_ID(N'[dbo].[FK__Buyers__BuyerSta__2CBDA3B5]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Buyers] DROP CONSTRAINT [FK__Buyers__BuyerSta__2CBDA3B5];
GO
IF OBJECT_ID(N'[dbo].[FK__Buyers__CountryI__2DB1C7EE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Buyers] DROP CONSTRAINT [FK__Buyers__CountryI__2DB1C7EE];
GO
IF OBJECT_ID(N'[dbo].[FK__Buyers_Compa__Id__2EA5EC27]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Buyers_CompanyBuyer] DROP CONSTRAINT [FK__Buyers_Compa__Id__2EA5EC27];
GO
IF OBJECT_ID(N'[dbo].[FK__Buyers_Indiv__Id__2F9A1060]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Buyers_IndividualBuyer] DROP CONSTRAINT [FK__Buyers_Indiv__Id__2F9A1060];
GO
IF OBJECT_ID(N'[dbo].[FK__Declarati__Buyer__336AA144]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DeclarationShipments] DROP CONSTRAINT [FK__Declarati__Buyer__336AA144];
GO
IF OBJECT_ID(N'[dbo].[FK__Declarati__Commo__345EC57D]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DeclarationShipments] DROP CONSTRAINT [FK__Declarati__Commo__345EC57D];
GO
IF OBJECT_ID(N'[dbo].[FK__Declarati__Count__3552E9B6]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DeclarationShipments] DROP CONSTRAINT [FK__Declarati__Count__3552E9B6];
GO
IF OBJECT_ID(N'[dbo].[FK__Declarati__Decla__308E3499]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Declarations] DROP CONSTRAINT [FK__Declarati__Decla__308E3499];
GO
IF OBJECT_ID(N'[dbo].[FK__Declarati__Decla__36470DEF]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DeclarationShipments] DROP CONSTRAINT [FK__Declarati__Decla__36470DEF];
GO
IF OBJECT_ID(N'[dbo].[FK__Declarati__Expor__318258D2]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Declarations] DROP CONSTRAINT [FK__Declarati__Expor__318258D2];
GO
IF OBJECT_ID(N'[dbo].[FK__Declarati__Polic__32767D0B]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Declarations] DROP CONSTRAINT [FK__Declarati__Polic__32767D0B];
GO
IF OBJECT_ID(N'[dbo].[FK__Declarati__TermO__373B3228]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DeclarationShipments] DROP CONSTRAINT [FK__Declarati__TermO__373B3228];
GO
IF OBJECT_ID(N'[dbo].[FK__ExporterD__Direc__382F5661]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ExporterDirector] DROP CONSTRAINT [FK__ExporterD__Direc__382F5661];
GO
IF OBJECT_ID(N'[dbo].[FK__ExporterD__Expor__39237A9A]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ExporterDirector] DROP CONSTRAINT [FK__ExporterD__Expor__39237A9A];
GO
IF OBJECT_ID(N'[dbo].[FK__Exporters__Expor__3A179ED3]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Exporters] DROP CONSTRAINT [FK__Exporters__Expor__3A179ED3];
GO
IF OBJECT_ID(N'[dbo].[FK__PastShipm__Buyer__3B0BC30C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PastShipments] DROP CONSTRAINT [FK__PastShipm__Buyer__3B0BC30C];
GO
IF OBJECT_ID(N'[dbo].[FK__PastShipm__Commo__3BFFE745]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PastShipments] DROP CONSTRAINT [FK__PastShipm__Commo__3BFFE745];
GO
IF OBJECT_ID(N'[dbo].[FK__PastShipm__Count__3DE82FB7]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PastShipments] DROP CONSTRAINT [FK__PastShipm__Count__3DE82FB7];
GO
IF OBJECT_ID(N'[dbo].[FK__PastShipm__Expor__3CF40B7E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PastShipments] DROP CONSTRAINT [FK__PastShipm__Expor__3CF40B7E];
GO
IF OBJECT_ID(N'[dbo].[FK__Policies__Export__3EDC53F0]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Policies] DROP CONSTRAINT [FK__Policies__Export__3EDC53F0];
GO
IF OBJECT_ID(N'[dbo].[FK__policyShi__Buyer__3FD07829]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[policyShipments] DROP CONSTRAINT [FK__policyShi__Buyer__3FD07829];
GO
IF OBJECT_ID(N'[dbo].[FK__policyShi__Commo__40C49C62]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[policyShipments] DROP CONSTRAINT [FK__policyShi__Commo__40C49C62];
GO
IF OBJECT_ID(N'[dbo].[FK__policyShi__Count__41B8C09B]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[policyShipments] DROP CONSTRAINT [FK__policyShi__Count__41B8C09B];
GO
IF OBJECT_ID(N'[dbo].[FK__policyShi__Polic__42ACE4D4]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[policyShipments] DROP CONSTRAINT [FK__policyShi__Polic__42ACE4D4];
GO
IF OBJECT_ID(N'[dbo].[FK__PremiumRa__Commo__43A1090D]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PremiumRates] DROP CONSTRAINT [FK__PremiumRa__Commo__43A1090D];
GO
IF OBJECT_ID(N'[dbo].[FK__PremiumRa__Count__44952D46]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PremiumRates] DROP CONSTRAINT [FK__PremiumRa__Count__44952D46];
GO
IF OBJECT_ID(N'[dbo].[FK__Shipments__Buyer__4589517F]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Shipments] DROP CONSTRAINT [FK__Shipments__Buyer__4589517F];
GO
IF OBJECT_ID(N'[dbo].[FK__Shipments__Commo__467D75B8]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Shipments] DROP CONSTRAINT [FK__Shipments__Commo__467D75B8];
GO
IF OBJECT_ID(N'[dbo].[FK__Shipments__Count__4865BE2A]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Shipments] DROP CONSTRAINT [FK__Shipments__Count__4865BE2A];
GO
IF OBJECT_ID(N'[dbo].[FK__Shipments__Expor__477199F1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Shipments] DROP CONSTRAINT [FK__Shipments__Expor__477199F1];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AccessControlAccessGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccessControlAccessGroup];
GO
IF OBJECT_ID(N'[dbo].[AccessControls]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccessControls];
GO
IF OBJECT_ID(N'[dbo].[AccessGroups]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccessGroups];
GO
IF OBJECT_ID(N'[dbo].[AccessGroupUser]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccessGroupUser];
GO
IF OBJECT_ID(N'[dbo].[Buyers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Buyers];
GO
IF OBJECT_ID(N'[dbo].[Buyers_CompanyBuyer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Buyers_CompanyBuyer];
GO
IF OBJECT_ID(N'[dbo].[Buyers_IndividualBuyer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Buyers_IndividualBuyer];
GO
IF OBJECT_ID(N'[dbo].[BuyerStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BuyerStatus];
GO
IF OBJECT_ID(N'[dbo].[Commodities]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Commodities];
GO
IF OBJECT_ID(N'[dbo].[Countries]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Countries];
GO
IF OBJECT_ID(N'[dbo].[Declarations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Declarations];
GO
IF OBJECT_ID(N'[dbo].[DeclarationShipments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeclarationShipments];
GO
IF OBJECT_ID(N'[dbo].[DeclarationStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeclarationStatus];
GO
IF OBJECT_ID(N'[dbo].[Directors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Directors];
GO
IF OBJECT_ID(N'[dbo].[ExporterDirector]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ExporterDirector];
GO
IF OBJECT_ID(N'[dbo].[Exporters]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Exporters];
GO
IF OBJECT_ID(N'[dbo].[ExporterStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ExporterStatus];
GO
IF OBJECT_ID(N'[dbo].[PastShipments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PastShipments];
GO
IF OBJECT_ID(N'[dbo].[Policies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Policies];
GO
IF OBJECT_ID(N'[dbo].[policyShipments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[policyShipments];
GO
IF OBJECT_ID(N'[dbo].[PremiumRates]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PremiumRates];
GO
IF OBJECT_ID(N'[dbo].[Shipments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Shipments];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[TermOfPayments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TermOfPayments];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Exporters'
CREATE TABLE [dbo].[Exporters] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [RegNo] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [TelNo] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [ShortTag] nvarchar(max)  NOT NULL,
    [ExporterStatusId] int  NOT NULL,
    [MaxLiability] nvarchar(max)  NULL
);
GO

-- Creating table 'Directors'
CREATE TABLE [dbo].[Directors] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NULL,
    [Address] nvarchar(max)  NULL,
    [NIC] nvarchar(max)  NULL,
    [TelNo] nvarchar(max)  NULL,
    [Email] nvarchar(max)  NULL,
    [JoinedDate] datetime  NULL,
    [Type] nvarchar(max)  NULL,
    [PassportNo] nvarchar(max)  NULL,
    [Gender] nvarchar(max)  NULL,
    [DateOfBirth] datetime  NULL,
    [LastName] nvarchar(max)  NULL
);
GO

-- Creating table 'Shipments'
CREATE TABLE [dbo].[Shipments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExporterId] int  NOT NULL,
    [BuyerId] int  NOT NULL,
    [CommodityId] int  NOT NULL,
    [ShipmentValue] float  NULL,
    [Country_Id] int  NOT NULL
);
GO

-- Creating table 'Buyers'
CREATE TABLE [dbo].[Buyers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CountryId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [City] nvarchar(max)  NOT NULL,
    [ZipCode] nvarchar(max)  NOT NULL,
    [Fax] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [TelephoneNo] nvarchar(max)  NOT NULL,
    [MaxLiability] nvarchar(max)  NULL,
    [BuyerStatus_Id] int  NOT NULL
);
GO

-- Creating table 'Declarations'
CREATE TABLE [dbo].[Declarations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExporterId] int  NOT NULL,
    [PolicyId] int  NOT NULL,
    [StampDate] datetime  NOT NULL,
    [DeclarationStatusId] int  NOT NULL,
    [Comment] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Policies'
CREATE TABLE [dbo].[Policies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PolicyNumber] nvarchar(max)  NOT NULL,
    [Duration] int  NULL,
    [Status] nvarchar(max)  NOT NULL,
    [RecievedDate] datetime  NULL,
    [StartDate] datetime  NULL,
    [ExporterId] int  NOT NULL,
    [Value] float  NOT NULL
);
GO

-- Creating table 'Commodities'
CREATE TABLE [dbo].[Commodities] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Countries'
CREATE TABLE [dbo].[Countries] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ShortCode] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PremiumRates'
CREATE TABLE [dbo].[PremiumRates] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Rate] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [CountryId] int  NOT NULL,
    [CommodityId] int  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EpfNo] nvarchar(max)  NOT NULL,
    [NIC] nvarchar(max)  NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Gender] nvarchar(max)  NOT NULL,
    [Designation] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [MobileNo] nvarchar(max)  NOT NULL,
    [LandPhone] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [AddedDate] datetime  NULL,
    [LastLoginDate] datetime  NULL,
    [DateOfBirth] datetime  NULL
);
GO

-- Creating table 'AccessGroups'
CREATE TABLE [dbo].[AccessGroups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AccessControls'
CREATE TABLE [dbo].[AccessControls] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ActionDescription] nvarchar(max)  NOT NULL,
    [ShortTag] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PastShipments'
CREATE TABLE [dbo].[PastShipments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Value] float  NOT NULL,
    [Buyer_Id] int  NOT NULL,
    [Commodity_Id] int  NOT NULL,
    [Exporter_Id] int  NOT NULL,
    [Country_Id] int  NOT NULL
);
GO

-- Creating table 'ExporterStatus'
CREATE TABLE [dbo].[ExporterStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'BuyerStatus'
CREATE TABLE [dbo].[BuyerStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'policyShipments'
CREATE TABLE [dbo].[policyShipments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BuyerId] int  NOT NULL,
    [CommodityId] int  NOT NULL,
    [CountryId] int  NOT NULL,
    [PolicyId] int  NOT NULL
);
GO

-- Creating table 'DeclarationShipments'
CREATE TABLE [dbo].[DeclarationShipments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [GrossValue] int  NOT NULL,
    [ShipmentDate] datetime  NOT NULL,
    [TermOfPayment] nvarchar(max)  NOT NULL,
    [CreditDuration] int  NOT NULL,
    [DeclarationId] int  NOT NULL,
    [BuyerId] int  NOT NULL,
    [CountryId] int  NOT NULL,
    [CommodityId] int  NOT NULL,
    [TermOfPaymentId] int  NOT NULL
);
GO

-- Creating table 'DeclarationStatus'
CREATE TABLE [dbo].[DeclarationStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'TermOfPayments'
CREATE TABLE [dbo].[TermOfPayments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Buyers_CompanyBuyer'
CREATE TABLE [dbo].[Buyers_CompanyBuyer] (
    [ShortName] nvarchar(max)  NOT NULL,
    [RegNo] nvarchar(max)  NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Buyers_IndividualBuyer'
CREATE TABLE [dbo].[Buyers_IndividualBuyer] (
    [DateOfBirth] datetime  NULL,
    [NIC] nvarchar(max)  NOT NULL,
    [MobileNo] nvarchar(max)  NOT NULL,
    [Passport] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'ExporterDirector'
CREATE TABLE [dbo].[ExporterDirector] (
    [Exporters_Id] int  NOT NULL,
    [Directors_Id] int  NOT NULL
);
GO

-- Creating table 'AccessGroupUser'
CREATE TABLE [dbo].[AccessGroupUser] (
    [AccessGroups_Id] int  NOT NULL,
    [Users_Id] int  NOT NULL
);
GO

-- Creating table 'AccessControlAccessGroup'
CREATE TABLE [dbo].[AccessControlAccessGroup] (
    [AccessControls_Id] int  NOT NULL,
    [AccessGroups_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Exporters'
ALTER TABLE [dbo].[Exporters]
ADD CONSTRAINT [PK_Exporters]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Directors'
ALTER TABLE [dbo].[Directors]
ADD CONSTRAINT [PK_Directors]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Shipments'
ALTER TABLE [dbo].[Shipments]
ADD CONSTRAINT [PK_Shipments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Buyers'
ALTER TABLE [dbo].[Buyers]
ADD CONSTRAINT [PK_Buyers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Declarations'
ALTER TABLE [dbo].[Declarations]
ADD CONSTRAINT [PK_Declarations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Policies'
ALTER TABLE [dbo].[Policies]
ADD CONSTRAINT [PK_Policies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Commodities'
ALTER TABLE [dbo].[Commodities]
ADD CONSTRAINT [PK_Commodities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Countries'
ALTER TABLE [dbo].[Countries]
ADD CONSTRAINT [PK_Countries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PremiumRates'
ALTER TABLE [dbo].[PremiumRates]
ADD CONSTRAINT [PK_PremiumRates]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AccessGroups'
ALTER TABLE [dbo].[AccessGroups]
ADD CONSTRAINT [PK_AccessGroups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AccessControls'
ALTER TABLE [dbo].[AccessControls]
ADD CONSTRAINT [PK_AccessControls]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PastShipments'
ALTER TABLE [dbo].[PastShipments]
ADD CONSTRAINT [PK_PastShipments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExporterStatus'
ALTER TABLE [dbo].[ExporterStatus]
ADD CONSTRAINT [PK_ExporterStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BuyerStatus'
ALTER TABLE [dbo].[BuyerStatus]
ADD CONSTRAINT [PK_BuyerStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'policyShipments'
ALTER TABLE [dbo].[policyShipments]
ADD CONSTRAINT [PK_policyShipments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DeclarationShipments'
ALTER TABLE [dbo].[DeclarationShipments]
ADD CONSTRAINT [PK_DeclarationShipments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DeclarationStatus'
ALTER TABLE [dbo].[DeclarationStatus]
ADD CONSTRAINT [PK_DeclarationStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [Id] in table 'TermOfPayments'
ALTER TABLE [dbo].[TermOfPayments]
ADD CONSTRAINT [PK_TermOfPayments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Buyers_CompanyBuyer'
ALTER TABLE [dbo].[Buyers_CompanyBuyer]
ADD CONSTRAINT [PK_Buyers_CompanyBuyer]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Buyers_IndividualBuyer'
ALTER TABLE [dbo].[Buyers_IndividualBuyer]
ADD CONSTRAINT [PK_Buyers_IndividualBuyer]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Exporters_Id], [Directors_Id] in table 'ExporterDirector'
ALTER TABLE [dbo].[ExporterDirector]
ADD CONSTRAINT [PK_ExporterDirector]
    PRIMARY KEY NONCLUSTERED ([Exporters_Id], [Directors_Id] ASC);
GO

-- Creating primary key on [AccessGroups_Id], [Users_Id] in table 'AccessGroupUser'
ALTER TABLE [dbo].[AccessGroupUser]
ADD CONSTRAINT [PK_AccessGroupUser]
    PRIMARY KEY NONCLUSTERED ([AccessGroups_Id], [Users_Id] ASC);
GO

-- Creating primary key on [AccessControls_Id], [AccessGroups_Id] in table 'AccessControlAccessGroup'
ALTER TABLE [dbo].[AccessControlAccessGroup]
ADD CONSTRAINT [PK_AccessControlAccessGroup]
    PRIMARY KEY NONCLUSTERED ([AccessControls_Id], [AccessGroups_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Exporters_Id] in table 'ExporterDirector'
ALTER TABLE [dbo].[ExporterDirector]
ADD CONSTRAINT [FK_ExporterDirector_Exporter]
    FOREIGN KEY ([Exporters_Id])
    REFERENCES [dbo].[Exporters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Directors_Id] in table 'ExporterDirector'
ALTER TABLE [dbo].[ExporterDirector]
ADD CONSTRAINT [FK_ExporterDirector_Director]
    FOREIGN KEY ([Directors_Id])
    REFERENCES [dbo].[Directors]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExporterDirector_Director'
CREATE INDEX [IX_FK_ExporterDirector_Director]
ON [dbo].[ExporterDirector]
    ([Directors_Id]);
GO

-- Creating foreign key on [ExporterId] in table 'Shipments'
ALTER TABLE [dbo].[Shipments]
ADD CONSTRAINT [FK_ExporterShipment]
    FOREIGN KEY ([ExporterId])
    REFERENCES [dbo].[Exporters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExporterShipment'
CREATE INDEX [IX_FK_ExporterShipment]
ON [dbo].[Shipments]
    ([ExporterId]);
GO

-- Creating foreign key on [ExporterId] in table 'Declarations'
ALTER TABLE [dbo].[Declarations]
ADD CONSTRAINT [FK_ExporterDeclaration]
    FOREIGN KEY ([ExporterId])
    REFERENCES [dbo].[Exporters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExporterDeclaration'
CREATE INDEX [IX_FK_ExporterDeclaration]
ON [dbo].[Declarations]
    ([ExporterId]);
GO

-- Creating foreign key on [BuyerId] in table 'Shipments'
ALTER TABLE [dbo].[Shipments]
ADD CONSTRAINT [FK_BuyerShipment]
    FOREIGN KEY ([BuyerId])
    REFERENCES [dbo].[Buyers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BuyerShipment'
CREATE INDEX [IX_FK_BuyerShipment]
ON [dbo].[Shipments]
    ([BuyerId]);
GO

-- Creating foreign key on [PolicyId] in table 'Declarations'
ALTER TABLE [dbo].[Declarations]
ADD CONSTRAINT [FK_PolicyDeclaration]
    FOREIGN KEY ([PolicyId])
    REFERENCES [dbo].[Policies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PolicyDeclaration'
CREATE INDEX [IX_FK_PolicyDeclaration]
ON [dbo].[Declarations]
    ([PolicyId]);
GO

-- Creating foreign key on [CommodityId] in table 'Shipments'
ALTER TABLE [dbo].[Shipments]
ADD CONSTRAINT [FK_CommodityShipment]
    FOREIGN KEY ([CommodityId])
    REFERENCES [dbo].[Commodities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CommodityShipment'
CREATE INDEX [IX_FK_CommodityShipment]
ON [dbo].[Shipments]
    ([CommodityId]);
GO

-- Creating foreign key on [CountryId] in table 'PremiumRates'
ALTER TABLE [dbo].[PremiumRates]
ADD CONSTRAINT [FK_CountryPremiumRate]
    FOREIGN KEY ([CountryId])
    REFERENCES [dbo].[Countries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CountryPremiumRate'
CREATE INDEX [IX_FK_CountryPremiumRate]
ON [dbo].[PremiumRates]
    ([CountryId]);
GO

-- Creating foreign key on [AccessGroups_Id] in table 'AccessGroupUser'
ALTER TABLE [dbo].[AccessGroupUser]
ADD CONSTRAINT [FK_AccessGroupUser_AccessGroup]
    FOREIGN KEY ([AccessGroups_Id])
    REFERENCES [dbo].[AccessGroups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_Id] in table 'AccessGroupUser'
ALTER TABLE [dbo].[AccessGroupUser]
ADD CONSTRAINT [FK_AccessGroupUser_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AccessGroupUser_User'
CREATE INDEX [IX_FK_AccessGroupUser_User]
ON [dbo].[AccessGroupUser]
    ([Users_Id]);
GO

-- Creating foreign key on [AccessControls_Id] in table 'AccessControlAccessGroup'
ALTER TABLE [dbo].[AccessControlAccessGroup]
ADD CONSTRAINT [FK_AccessControlAccessGroup_AccessControl]
    FOREIGN KEY ([AccessControls_Id])
    REFERENCES [dbo].[AccessControls]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AccessGroups_Id] in table 'AccessControlAccessGroup'
ALTER TABLE [dbo].[AccessControlAccessGroup]
ADD CONSTRAINT [FK_AccessControlAccessGroup_AccessGroup]
    FOREIGN KEY ([AccessGroups_Id])
    REFERENCES [dbo].[AccessGroups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AccessControlAccessGroup_AccessGroup'
CREATE INDEX [IX_FK_AccessControlAccessGroup_AccessGroup]
ON [dbo].[AccessControlAccessGroup]
    ([AccessGroups_Id]);
GO

-- Creating foreign key on [CountryId] in table 'Buyers'
ALTER TABLE [dbo].[Buyers]
ADD CONSTRAINT [FK_BuyerCountry]
    FOREIGN KEY ([CountryId])
    REFERENCES [dbo].[Countries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BuyerCountry'
CREATE INDEX [IX_FK_BuyerCountry]
ON [dbo].[Buyers]
    ([CountryId]);
GO

-- Creating foreign key on [CommodityId] in table 'PremiumRates'
ALTER TABLE [dbo].[PremiumRates]
ADD CONSTRAINT [FK_CommodityPremiumRate]
    FOREIGN KEY ([CommodityId])
    REFERENCES [dbo].[Commodities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CommodityPremiumRate'
CREATE INDEX [IX_FK_CommodityPremiumRate]
ON [dbo].[PremiumRates]
    ([CommodityId]);
GO

-- Creating foreign key on [Buyer_Id] in table 'PastShipments'
ALTER TABLE [dbo].[PastShipments]
ADD CONSTRAINT [FK_Past_ShipmentBuyer]
    FOREIGN KEY ([Buyer_Id])
    REFERENCES [dbo].[Buyers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Past_ShipmentBuyer'
CREATE INDEX [IX_FK_Past_ShipmentBuyer]
ON [dbo].[PastShipments]
    ([Buyer_Id]);
GO

-- Creating foreign key on [Commodity_Id] in table 'PastShipments'
ALTER TABLE [dbo].[PastShipments]
ADD CONSTRAINT [FK_Past_ShipmentCommodity]
    FOREIGN KEY ([Commodity_Id])
    REFERENCES [dbo].[Commodities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Past_ShipmentCommodity'
CREATE INDEX [IX_FK_Past_ShipmentCommodity]
ON [dbo].[PastShipments]
    ([Commodity_Id]);
GO

-- Creating foreign key on [Exporter_Id] in table 'PastShipments'
ALTER TABLE [dbo].[PastShipments]
ADD CONSTRAINT [FK_Past_ShipmentExporter]
    FOREIGN KEY ([Exporter_Id])
    REFERENCES [dbo].[Exporters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Past_ShipmentExporter'
CREATE INDEX [IX_FK_Past_ShipmentExporter]
ON [dbo].[PastShipments]
    ([Exporter_Id]);
GO

-- Creating foreign key on [Country_Id] in table 'PastShipments'
ALTER TABLE [dbo].[PastShipments]
ADD CONSTRAINT [FK_PastShipmentCountry]
    FOREIGN KEY ([Country_Id])
    REFERENCES [dbo].[Countries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PastShipmentCountry'
CREATE INDEX [IX_FK_PastShipmentCountry]
ON [dbo].[PastShipments]
    ([Country_Id]);
GO

-- Creating foreign key on [ExporterStatusId] in table 'Exporters'
ALTER TABLE [dbo].[Exporters]
ADD CONSTRAINT [FK_ExporterStatusExporter]
    FOREIGN KEY ([ExporterStatusId])
    REFERENCES [dbo].[ExporterStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExporterStatusExporter'
CREATE INDEX [IX_FK_ExporterStatusExporter]
ON [dbo].[Exporters]
    ([ExporterStatusId]);
GO

-- Creating foreign key on [ExporterId] in table 'Policies'
ALTER TABLE [dbo].[Policies]
ADD CONSTRAINT [FK_PolicyExporter]
    FOREIGN KEY ([ExporterId])
    REFERENCES [dbo].[Exporters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PolicyExporter'
CREATE INDEX [IX_FK_PolicyExporter]
ON [dbo].[Policies]
    ([ExporterId]);
GO

-- Creating foreign key on [Country_Id] in table 'Shipments'
ALTER TABLE [dbo].[Shipments]
ADD CONSTRAINT [FK_ShipmentCountry]
    FOREIGN KEY ([Country_Id])
    REFERENCES [dbo].[Countries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ShipmentCountry'
CREATE INDEX [IX_FK_ShipmentCountry]
ON [dbo].[Shipments]
    ([Country_Id]);
GO

-- Creating foreign key on [BuyerStatus_Id] in table 'Buyers'
ALTER TABLE [dbo].[Buyers]
ADD CONSTRAINT [FK_BuyerBuyerStatus]
    FOREIGN KEY ([BuyerStatus_Id])
    REFERENCES [dbo].[BuyerStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BuyerBuyerStatus'
CREATE INDEX [IX_FK_BuyerBuyerStatus]
ON [dbo].[Buyers]
    ([BuyerStatus_Id]);
GO

-- Creating foreign key on [BuyerId] in table 'policyShipments'
ALTER TABLE [dbo].[policyShipments]
ADD CONSTRAINT [FK_policyShipmentBuyer]
    FOREIGN KEY ([BuyerId])
    REFERENCES [dbo].[Buyers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_policyShipmentBuyer'
CREATE INDEX [IX_FK_policyShipmentBuyer]
ON [dbo].[policyShipments]
    ([BuyerId]);
GO

-- Creating foreign key on [CommodityId] in table 'policyShipments'
ALTER TABLE [dbo].[policyShipments]
ADD CONSTRAINT [FK_policyShipmentCommodity]
    FOREIGN KEY ([CommodityId])
    REFERENCES [dbo].[Commodities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_policyShipmentCommodity'
CREATE INDEX [IX_FK_policyShipmentCommodity]
ON [dbo].[policyShipments]
    ([CommodityId]);
GO

-- Creating foreign key on [CountryId] in table 'policyShipments'
ALTER TABLE [dbo].[policyShipments]
ADD CONSTRAINT [FK_policyShipmentCountry]
    FOREIGN KEY ([CountryId])
    REFERENCES [dbo].[Countries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_policyShipmentCountry'
CREATE INDEX [IX_FK_policyShipmentCountry]
ON [dbo].[policyShipments]
    ([CountryId]);
GO

-- Creating foreign key on [PolicyId] in table 'policyShipments'
ALTER TABLE [dbo].[policyShipments]
ADD CONSTRAINT [FK_policyShipmentPolicy]
    FOREIGN KEY ([PolicyId])
    REFERENCES [dbo].[Policies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_policyShipmentPolicy'
CREATE INDEX [IX_FK_policyShipmentPolicy]
ON [dbo].[policyShipments]
    ([PolicyId]);
GO

-- Creating foreign key on [DeclarationId] in table 'DeclarationShipments'
ALTER TABLE [dbo].[DeclarationShipments]
ADD CONSTRAINT [FK_DeclarationShipmentDeclaration]
    FOREIGN KEY ([DeclarationId])
    REFERENCES [dbo].[Declarations]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DeclarationShipmentDeclaration'
CREATE INDEX [IX_FK_DeclarationShipmentDeclaration]
ON [dbo].[DeclarationShipments]
    ([DeclarationId]);
GO

-- Creating foreign key on [BuyerId] in table 'DeclarationShipments'
ALTER TABLE [dbo].[DeclarationShipments]
ADD CONSTRAINT [FK_DeclarationShipmentBuyer]
    FOREIGN KEY ([BuyerId])
    REFERENCES [dbo].[Buyers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DeclarationShipmentBuyer'
CREATE INDEX [IX_FK_DeclarationShipmentBuyer]
ON [dbo].[DeclarationShipments]
    ([BuyerId]);
GO

-- Creating foreign key on [CountryId] in table 'DeclarationShipments'
ALTER TABLE [dbo].[DeclarationShipments]
ADD CONSTRAINT [FK_DeclarationShipmentCountry]
    FOREIGN KEY ([CountryId])
    REFERENCES [dbo].[Countries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DeclarationShipmentCountry'
CREATE INDEX [IX_FK_DeclarationShipmentCountry]
ON [dbo].[DeclarationShipments]
    ([CountryId]);
GO

-- Creating foreign key on [CommodityId] in table 'DeclarationShipments'
ALTER TABLE [dbo].[DeclarationShipments]
ADD CONSTRAINT [FK_DeclarationShipmentCommodity]
    FOREIGN KEY ([CommodityId])
    REFERENCES [dbo].[Commodities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DeclarationShipmentCommodity'
CREATE INDEX [IX_FK_DeclarationShipmentCommodity]
ON [dbo].[DeclarationShipments]
    ([CommodityId]);
GO

-- Creating foreign key on [DeclarationStatusId] in table 'Declarations'
ALTER TABLE [dbo].[Declarations]
ADD CONSTRAINT [FK__Declarati__Decla__308E3499]
    FOREIGN KEY ([DeclarationStatusId])
    REFERENCES [dbo].[DeclarationStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Declarati__Decla__308E3499'
CREATE INDEX [IX_FK__Declarati__Decla__308E3499]
ON [dbo].[Declarations]
    ([DeclarationStatusId]);
GO

-- Creating foreign key on [TermOfPaymentId] in table 'DeclarationShipments'
ALTER TABLE [dbo].[DeclarationShipments]
ADD CONSTRAINT [FK__Declarati__TermO__373B3228]
    FOREIGN KEY ([TermOfPaymentId])
    REFERENCES [dbo].[TermOfPayments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Declarati__TermO__373B3228'
CREATE INDEX [IX_FK__Declarati__TermO__373B3228]
ON [dbo].[DeclarationShipments]
    ([TermOfPaymentId]);
GO

-- Creating foreign key on [Id] in table 'Buyers_CompanyBuyer'
ALTER TABLE [dbo].[Buyers_CompanyBuyer]
ADD CONSTRAINT [FK_CompanyBuyer_inherits_Buyer]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Buyers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Buyers_IndividualBuyer'
ALTER TABLE [dbo].[Buyers_IndividualBuyer]
ADD CONSTRAINT [FK_IndividualBuyer_inherits_Buyer]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Buyers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
---- --------------------------------------------------