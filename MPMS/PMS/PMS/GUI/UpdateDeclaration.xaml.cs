﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS.GUI
{
    /// <summary>
    /// Interaction logic for UpdateDeclaration.xaml
    /// </summary>
    public partial class UpdateDeclaration : Page
    {
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        private Declaration declaration;
        public UpdateDeclaration()
        {
            InitializeComponent();
        }
        
        
       
        public UpdateDeclaration(Declaration dec)
        {

            this.InitializeComponent();
            declaration = model.Declarations.Where(u => u.Id == dec.Id).FirstOrDefault();
            fillExporter();
            fillPolicy();
            fillGrid();
            loadDetails();


        
        
        }

        //loading values to the fields
        private void loadDetails()
        {
            cmbExporter.SelectedItem = declaration.Exporter;
            dtPicker_StampDate.SelectedDate = declaration.StampDate;
            cmb_policyNumber.SelectedItem = declaration.Policy;
        
        }

        //method to fill the shipment data grid
        private void fillGrid()
        {
            if (declaration != null)
            {
                IEnumerable<DeclarationShipment> shipments = model.DeclarationShipments;
                shipments = shipments.Where(u => u.DeclarationId.Equals(declaration.Id));
                grdDeclarationShipment.ItemsSource = shipments;

            }
        
        
        }
      

        //Fill Policy
        private void fillPolicy()
        {
            cmb_policyNumber.ItemsSource = model.Policies;
            cmb_policyNumber.DisplayMemberPath = "PolicyNumber";
        }

        //Fill Exporter
        private void fillExporter()
        {
            cmbExporter.ItemsSource = model.Exporters;
            cmbExporter.DisplayMemberPath = "Name";
        }

      

       

        //save button click event
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            declaration.Exporter = (Exporter)cmbExporter.SelectedItem;
            declaration.Policy = (Policy)cmb_policyNumber.SelectedItem;
            declaration.StampDate = dtPicker_StampDate.SelectedDate.Value;
            

            int rows = model.SaveChanges();

            if (rows == -1)
            {
                MessageBox.Show("Failed to save the record");
                return;
            }
            MessageBox.Show("Successfully saved the record");

            loadDetails();
           
        }

        //remove button click event
        private void btnRemove_Click_1(object sender, RoutedEventArgs e)
        {
            var dec = model.Declarations.Where(i => i.Id == declaration.Id).FirstOrDefault();
            model.Declarations.DeleteObject(dec);
            model.SaveChanges();
        }


        private void grdDeclarationShipment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedShipment = grdDeclarationShipment.SelectedItem;

            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new UpdateDeclarationShipments((DeclarationShipment)selectedShipment);

        }
    }
}
