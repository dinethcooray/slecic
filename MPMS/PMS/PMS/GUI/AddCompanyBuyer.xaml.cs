﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PMS
{
	public partial class AddCompanyBuyer
	{
        PMS_Main_ModelContainer model;
		public AddCompanyBuyer()
		{
			this.InitializeComponent();
            model = new PMS_Main_ModelContainer();
			// Insert code required on object creation below this point.
		}

        private void btnCAdd_Click(object sender, RoutedEventArgs e)
        {
            CompanyBuyer companybuyer = new CompanyBuyer();
            companybuyer.Country = (Country)comboBoxCCountry.SelectedItem;

            companybuyer.City = txtCCity.Text;
            companybuyer.Address = txtCAddress.Text;
            companybuyer.ZipCode = txtCZipCode.Text;
            companybuyer.Name = txtCName.Text;
            companybuyer.Fax = txtCFax.Text;
            companybuyer.Email = txtCEmail.Text;
            companybuyer.TelephoneNo = txtCTelephoneNo.Text;
            companybuyer.ShortName = txtCShortTag.Text;
            companybuyer.RegNo = txtCRegNo.Text;
            companybuyer.MaxLiability = txtCMaxLiability.Text;
            companybuyer.BuyerStatus =(BuyerStatus) comboxCStatus.SelectedItem;
            model.AddToBuyers(companybuyer);

            Validation v = new Validation();

            if (v.ValidateTextField(txtCName.Text))
            {

                if (v.ValidateEmail(txtCEmail.Text))
                {
                    model.AddToBuyers(companybuyer);
                    model.SaveChanges();
                    MessageBox.Show("Successfully added to database", "Added the company");

                }

                else
                    MessageBox.Show("Email is in Invalide formate", "Error");


            }
            else
            {
                MessageBox.Show("Need to enter details in required fields", "Error");

            }

        }
        private void BindCountry()
        {
            var countries = model.Countries;
            comboBoxCCountry.ItemsSource = countries;
            comboBoxCCountry.DisplayMemberPath = "Name";
           
        }


        private void BindStatus()
        {
            var status = model.BuyerStatus; 
            comboxCStatus.ItemsSource = status;
            comboxCStatus.DisplayMemberPath = "description";

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BindCountry();
            BindStatus();
        }
	}
}