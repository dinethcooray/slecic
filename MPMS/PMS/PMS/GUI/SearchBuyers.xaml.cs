﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;
using System.Reflection;

namespace PMS
{
	public partial class SearchBuyers
	{
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
		public SearchBuyers()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
       
       
            
            //obtatin search result and bind it into a gridview
                var buyers = model.Buyers.Where(buyer => buyer.Name.Contains(txtSearchkey.Text)).Select(user => new { user.Id,user.Name, user.TelephoneNo, user.ZipCode, user.Address, user.City, user.Country, user.Fax, user.Email, user.MaxLiability });
                dgBuyers.DataContext = buyers;
             

           
        }

        private void dgBuyers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = dgBuyers.SelectedItem;
            Type type = selected.GetType();
            PropertyInfo property = type.GetProperty("Id");
            int value = (int)property.GetValue(selected, null);

            ContentPresenter frame = (ContentPresenter)this.VisualParent;
            //frame.Content = new CompanyBuyerDetails(value);

        }
	}
}