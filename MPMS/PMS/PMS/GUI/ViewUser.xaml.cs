﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace PMS
{
    /// <summary>
    /// Interaction logic for ViewUser.xaml
    /// </summary>
    public partial class ViewUser : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Validation valid = new Validation();

        public ViewUser()
        {
            InitializeComponent();
            LoadGrid();
        }

        //Load data grid from database
        private void LoadGrid()
        {
            IEnumerable<User> users = model.Users;

            //epf
            if (txtSearchEpf.Text != "")
            {
                users = users.Where(u => u.EpfNo.Contains(txtSearchEpf.Text));
            }

            //designation
            if (txtSearchDes.Text != "")
            {
                users = users.Where(u => u.Designation.Contains(txtSearchDes.Text));
            }

            //firstname
            if (txtSearchFname.Text != "")
            {
                users = users.Where(u => u.FirstName.Contains(txtSearchFname.Text));
            }

            //username
            if (txtSearchUname.Text != "")
            {
                users = users.Where(u => u.UserName.Contains(txtSearchUname.Text));
            }

            //lastname
            if (txtSearchLname.Text != "")
            {
                users = users.Where(u => u.LastName.Contains(txtSearchLname.Text));
            }
            grdUsers.ItemsSource = users;
        }

        private void txtSearchEpf_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.LoadGrid();
        }

        private void txtSearchFname_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.LoadGrid();
        }

        private void txtSearchLname_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.LoadGrid();
        }

        private void txtSearchDes_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.LoadGrid();
        }

        private void txtSearchUname_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.LoadGrid();
        }

        private void btnRefesh_Click(object sender, RoutedEventArgs e)
        {
            
            LoadGrid();
        }

        private void grdUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            string value = ((User)grdUsers.SelectedItem).EpfNo;
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new ViewUserDetails(value);
        }

       
    }
}
