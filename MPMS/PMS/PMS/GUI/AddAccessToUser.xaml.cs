﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace PMS
{
    /// <summary>
    /// Interaction logic for AddAccessToUser.xaml
    /// </summary>
    public partial class AddAccessToUser : Page
    {
        //Create Entity model oblect
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();

        //Represents a dynamic data collection that provides notifications when items get added, removed, or when the whole list is refreshed.
        public ObservableCollection<BoolStringClass> TheList { get; set; }

      public AddAccessToUser()
      {
         InitializeComponent();
         TheList = new ObservableCollection<BoolStringClass>();

            //Load values to combobox
            txtUname.ItemsSource = model.Users;
            txtUname.DisplayMemberPath = "EpfNo";
            txtUname.SelectedItem = 1;
      }

      //When check add access group to User
      private void Incheck(object sender, RoutedEventArgs e)
      {
          //Get Checkedbox value
          CheckBox chkZone = (CheckBox)sender;
          User usr = (User)txtUname.SelectedItem;
          int accId = Convert.ToInt32(chkZone.Tag);

          //Get Access group object according to access id
          AccessGroup accGrp = model.AccessGroups.Where(accContrl => accContrl.Id == accId).First();

          //Add Access group to User
          usr.AccessGroups.Add(accGrp);
          model.SaveChanges();
      }

      //When Unchecked remove access group from user
      private void UnCheck(object sender, RoutedEventArgs e)
      {
          //Get Checkedboc value
          CheckBox chkZone = (CheckBox)sender;
          User usr = (User)txtUname.SelectedItem;
          int accId = Convert.ToInt32(chkZone.Tag);

          //Get Access group object according to access id
          AccessGroup accGrp = model.AccessGroups.Where(accContrl => accContrl.Id == accId).First();

          //Delete Access from group
          usr.AccessGroups.Remove(accGrp);
          model.SaveChanges();
      }

      //Check access to group
      private bool IsItCheck(int grpID)
      {

          bool result = false;
          User usr = (User)txtUname.SelectedItem;

          //Get object to access
          var accGrp = usr.AccessGroups.Where(groupID => groupID.Id == grpID);
          if (accGrp.Count() > 0)
          {
              result = true;
          }

          return result;
      }

      //Load checkbox list
      private void LoadAccessGroupList()
      {
          var accGrpList = (from g in model.AccessGroups select new { Name1 = g.Name, Id1 = g.Id });
          this.DataContext = accGrpList;

          foreach (var item in accGrpList)
          {
              TheList.Add(new BoolStringClass { Name = item.Name1, Id = item.Id1, IsSelected = IsItCheck(item.Id1) });
          }

          this.DataContext = this;
      }

       //When combobox change load access group to user
       private void txtUname_SelectionChanged(object sender, SelectionChangedEventArgs e)
       {
           TheList.Clear();
           LoadAccessGroupList();
       }

       private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
       {

       }

    }
        
}
