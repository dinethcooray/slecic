﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for ViewPastShipmentDetails.xaml
    /// </summary>
    public partial class ViewPastShipmentDetails : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();

        PastShipment pastShipment;

        public ViewPastShipmentDetails()
        {
            InitializeComponent();
        }

        public ViewPastShipmentDetails(PastShipment ps)
        {
            this.InitializeComponent();
            pastShipment = ps;
        
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).frame2.Content = new UpdatePastShipment(pastShipment);
        }

        private void page_loaded(object sender, RoutedEventArgs e)
        {
            lblBuyer.Content = pastShipment.Buyer.Name;
            lblCommodity.Content = pastShipment.Commodity.Name;
            lblExporter.Content = pastShipment.Exporter.Name;
            lblValue.Content = pastShipment.Value;
            lblDate.Content = pastShipment.Date;
            lblCountry.Content = pastShipment.Country.Name;

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
           
            //var pastShipment = model.PastShipments.Where(i => i.Id==pastShipment.Id).FirstOrDefault();

        }
    }
}
