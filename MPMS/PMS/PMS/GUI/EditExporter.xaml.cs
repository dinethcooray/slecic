﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PMS
{
	public partial class EditExporter
	{
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Exporter exporter;
		public EditExporter()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}

        public EditExporter(Exporter exp)
        {
            exporter = exp;
            InitializeComponent();
        
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            exporter.Address = txtAddress.Text;
            exporter.Description = txtDescription.Text;
            exporter.Email = txtEmail.Text;
            exporter.Name = txtName.Text;
            exporter.RegNo = txtRegNo.Text;
            exporter.ShortTag =txtShortTag.Text;
            exporter.TelNo=txtTelNo.Text;
            exporter.ExporterStatu = (ExporterStatus)cmbStatus.SelectedItem;

            model.SaveChanges();

            MessageBox.Show("Details updated successfully");

        }

        private void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {

        }

        private void page_loaded(object sender, RoutedEventArgs e)
        {
            //fill combo boxes
            cmbStatus.ItemsSource = model.ExporterStatus;
            cmbStatus.DisplayMemberPath = "Description";

            //fill details
            txtAddress.Text = exporter.Address;
            txtDescription.Text = exporter.Description;
            txtEmail.Text = exporter.Email;
            txtName.Text = exporter.Name;
            txtRegNo.Text = exporter.RegNo;
            txtShortTag.Text = exporter.ShortTag;
            txtTelNo.Text = exporter.TelNo;
            cmbStatus.SelectedItem = exporter.ExporterStatu;

            //data source for the datagrid 

            gridDirector.ItemsSource = exporter.Directors;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new AddDirectorsForExporters(this.exporter);
        }
	}
}