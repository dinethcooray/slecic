﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PMS
{
    public class CheckUserAccess
    {
        //Check User Access
        public static bool checkAccess(string access)
        {
            //Create Entity model oblect
            PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
            
            bool result = false;
            User user = null;
            try
            {
                user = model.Users.Where(userId => userId.Id == UserDetails.UserId).First();
            }
            catch
            {
                return false;
            }
            foreach (AccessGroup ag in user.AccessGroups)
            {
                int mm = ag.AccessControls.Where(accControl => accControl.ShortTag == access).Count();
                if (ag.AccessControls.Where(accControl => accControl.ShortTag == access).Count() > 0)
                {
                    result = true;
                }
            }

            return result;
        }
    }
}
