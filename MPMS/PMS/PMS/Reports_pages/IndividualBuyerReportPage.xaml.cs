﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
namespace PMS.Reports_pages
{
    /// <summary>
    /// Interaction logic for IndividualBuyerReportPage.xaml
    /// </summary>
    public partial class IndividualBuyerReportPage : Page
    {
        public IndividualBuyerReportPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ReportDocument report = new ReportDocument();
            report.Load("F:/PMS/PMS/PMS/PMS/Reports/IndividualBuyerReport.rpt");
            using (PMS_Main_ModelContainer db = new PMS_Main_ModelContainer())
            {
               


                report.SetDataSource(from i in db.Buyers.OfType<IndividualBuyer>()
                                     select new { i.Name, i.City,  i.TelephoneNo, i.MaxLiability,i.NIC,i.Email});

            }
            crystalReportsViewer1.ViewerCore.ReportSource = report;
        }

        private void crystalReportsViewer1_Loaded(object sender, RoutedEventArgs e)
        {

        }

        
    }
}
