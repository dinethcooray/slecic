﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace PMS.Reports_pages
{
    /// <summary>
    /// Interaction logic for companybuyerwindow.xaml
    /// </summary>
    public partial class companybuyerwindow : Window
    {
        String status;
        public companybuyerwindow(String pStatus)
        {
            InitializeComponent();

            this.status = pStatus;
        }

        private void crystalReportsViewer1_Loaded(object sender, RoutedEventArgs e)
        {
            ReportDocument report = new ReportDocument();
         //String loc=   System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
         //MessageBox.Show(loc);
            report.Load("../../Reports/CompanyBuyersReport.rpt");
         
          //  "C:/Users/supun/Desktop/8.12 - Copy - Copy/PMS/PMS/PMS/PMS/Reports/CompanyBuyersReport.rpt"
            using (PMS_Main_ModelContainer db = new PMS_Main_ModelContainer())
            {

              // report.SetDataSource(db.Buyers.OfType<CompanyBuyer>());

                var result = db.Buyers.OfType<CompanyBuyer>().Where(i => i.BuyerStatus.description == status);
                report.SetDataSource(result);

               // report.SetDataSource(from c in db.Buyers.OfType<CompanyBuyer>()
               //                      select new { c.Name, c.City, c.Email, c.TelephoneNo, c.MaxLiability });

            }
            crystalReportsViewer1.ViewerCore.ReportSource = report;
        }
    }
}
