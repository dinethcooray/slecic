﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CrystalDecisions.CrystalReports.Engine;
 using CrystalDecisions.Shared;

namespace PMS.Reports_pages
{
    /// <summary>
    /// Interaction logic for CompanyBuyersReportPage.xaml
    /// </summary>
    public partial class CompanyBuyersReportPage : Page
    {
       // public BindingStatus;
        public CompanyBuyersReportPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ReportDocument report = new ReportDocument();
            report.Load("F:/PMS/PMS/PMS/PMS/Reports/CompanyBuyersReport.rpt");
            using (PMS_Main_ModelContainer db = new PMS_Main_ModelContainer())
            {
                report.SetDataSource(db.Buyers.OfType<CompanyBuyer>());


                report.SetDataSource(from c in db.Buyers.OfType<CompanyBuyer>()
                                     select new { c.Name, c.City, c.Email,c.TelephoneNo, c.MaxLiability,c.BuyerStatus.description });

            }
               crystalReportsViewer1.ViewerCore.ReportSource = report;
            }

        private void crystalReportsViewer1_Loaded(object sender, RoutedEventArgs e)
        {

        }
        }
    }

