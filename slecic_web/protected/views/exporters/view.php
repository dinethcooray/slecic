<?php
$this->breadcrumbs=array(
	'Exporters'=>array('index'),
	$model->Name,
);

$this->menu=array(
	array('label'=>'Update My Profile', 'url'=>array('update', 'id'=>$model->Id)),
);
?>

<?php
$user = Exporters::model()->findByAttributes(array('Id'=>$model->Id))->Name;
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Name',
		'RegNo',
		'Address',
		'TelNo',
		'Email',
		'Description',
		'ShortTag',
		'MaxLiability',
		'username',
		'password',
		'last_login_date',
		'lmd',
		 array(
			'label'=>'Last Modified User',
			'value'=> $user,	
		),	
	),
)); ?>
