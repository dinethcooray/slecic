<?php
$this->breadcrumbs=array(
	'Exporters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Exporters', 'url'=>array('index')),
	array('label'=>'Create Exporters', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('exporters-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Exporters</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'exporters-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'Name',
		'RegNo',
		'Address',
		'TelNo',
		'Email',
		/*
		'Description',
		'ShortTag',
		'ExporterStatusId',
		'MaxLiability',
		'username',
		'password',
		'last_login_date',
		'lmd',
		'lmu',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
