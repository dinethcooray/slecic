<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RegNo')); ?>:</b>
	<?php echo CHtml::encode($data->RegNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Address')); ?>:</b>
	<?php echo CHtml::encode($data->Address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TelNo')); ?>:</b>
	<?php echo CHtml::encode($data->TelNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email')); ?>:</b>
	<?php echo CHtml::encode($data->Email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Description')); ?>:</b>
	<?php echo CHtml::encode($data->Description); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ShortTag')); ?>:</b>
	<?php echo CHtml::encode($data->ShortTag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ExporterStatusId')); ?>:</b>
	<?php echo CHtml::encode($data->ExporterStatusId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaxLiability')); ?>:</b>
	<?php echo CHtml::encode($data->MaxLiability); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_login_date')); ?>:</b>
	<?php echo CHtml::encode($data->last_login_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lmd')); ?>:</b>
	<?php echo CHtml::encode($data->lmd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lmu')); ?>:</b>
	<?php echo CHtml::encode($data->lmu); ?>
	<br />

	*/ ?>

</div>