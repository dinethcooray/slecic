<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exporters-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Name'); ?>
		<?php echo $form->textField($model,'Name',array('size'=>50,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RegNo'); ?>
		<?php echo $form->textField($model,'RegNo',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'RegNo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Address'); ?>
		<?php echo $form->textField($model,'Address',array('size'=>50,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'Address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TelNo'); ?>
		<?php echo $form->textField($model,'TelNo',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'TelNo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Description'); ?>
		<?php echo $form->textArea($model,'Description',array('rows'=>5, 'cols'=>38)); ?>
		<?php echo $form->error($model,'Description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ShortTag'); ?>
		<?php echo $form->textField($model,'ShortTag',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'ShortTag'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'MaxLiability'); ?>
		<?php echo $form->textField($model,'MaxLiability',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'MaxLiability'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->LabelEx($model,'confirmPassword'); ?>
		<?php echo $form->passwordField($model,'confirmPassword',array('size'=>50,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'confirmPassword'); ?>
	</div>    

	<div class="row">
		<?php date_default_timezone_set('Asia/Colombo'); echo $form->hiddenField($model,'lmd',array('value'=>date('Y-m-d h:i:s', time()))); ?>
	</div>

	<div class="row">
		<?php echo $form->hiddenField($model,'lmu',array('value'=>intval (Yii::app()->user->getId()))); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->