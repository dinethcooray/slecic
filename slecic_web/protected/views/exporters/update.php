<?php
$this->breadcrumbs=array(
	'Exporters'=>array('index'),
	$model->Name=>array('view','id'=>$model->Id),
	'Update',
);

$this->menu=array(
	array('label'=>'View My Profile', 'url'=>array('view', 'id'=>$model->Id)),
);
?>

<h1>Update My Profile</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>