<?php
$this->breadcrumbs=array(
	'Exporters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Exporters', 'url'=>array('index')),
	array('label'=>'Manage Exporters', 'url'=>array('admin')),
);
?>

<h1>Create Exporters</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>