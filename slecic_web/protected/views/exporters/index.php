<?php
$this->breadcrumbs=array(
	'Exporters',
);

$this->menu=array(
	array('label'=>'Create Exporters', 'url'=>array('create')),
	array('label'=>'Manage Exporters', 'url'=>array('admin')),
);
?>

<h1>Exporters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
