<?php
$this->breadcrumbs=array(
	'Declarations'=>array('index'),
	$model->Id=>array('view','id'=>$model->Id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage Declarations', 'url'=>array('admin')),
	array('label'=>'Add Declarations', 'url'=>array('create')),
	array('label'=>'View Declarations', 'url'=>array('view', 'id'=>$model->Id)),
);
$opolicies = Policies::model()->findByAttributes(array('Id'=>$model->PolicyId));
if(is_object($opolicies))
{
	$policies = $opolicies->PolicyNumber;
}
?>

<h1>Update Declaration <?php echo $policies; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>