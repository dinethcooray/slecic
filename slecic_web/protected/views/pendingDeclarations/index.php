<?php
$this->breadcrumbs=array(
	'Declarations',
);

$this->menu=array(
	array('label'=>'Create Declarations', 'url'=>array('create')),
	array('label'=>'Manage Declarations', 'url'=>array('admin')),
);
?>

<h1>Declarations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
