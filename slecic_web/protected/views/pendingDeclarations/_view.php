<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ExporterId')); ?>:</b>
	<?php echo CHtml::encode($data->ExporterId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BuyerId')); ?>:</b>
	<?php echo CHtml::encode($data->BuyerId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PolicyId')); ?>:</b>
	<?php echo CHtml::encode($data->PolicyId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CommodityId')); ?>:</b>
	<?php echo CHtml::encode($data->CommodityId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ShipmentDate')); ?>:</b>
	<?php echo CHtml::encode($data->ShipmentDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TermOfPayment')); ?>:</b>
	<?php echo CHtml::encode($data->TermOfPayment); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('StampDate')); ?>:</b>
	<?php echo CHtml::encode($data->StampDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CreditDuration')); ?>:</b>
	<?php echo CHtml::encode($data->CreditDuration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CountryId')); ?>:</b>
	<?php echo CHtml::encode($data->CountryId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GrossValue')); ?>:</b>
	<?php echo CHtml::encode($data->GrossValue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PremiumRateId')); ?>:</b>
	<?php echo CHtml::encode($data->PremiumRateId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Buyer_Id')); ?>:</b>
	<?php echo CHtml::encode($data->Buyer_Id); ?>
	<br />

	*/ ?>

</div>