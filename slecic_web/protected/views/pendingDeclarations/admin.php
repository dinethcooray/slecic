<?php
$this->breadcrumbs=array(
	'Declarations'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Add Declarations', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('declarations-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pending Declarations</h1>

<?php echo CHtml::link('Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'declarations-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		 array(
            'header'=>'Policy',
			'class'=> 'Policyname',	
		),
		'StampDate',
		'Comment',
		 array(
            'name' => 'Shipments',
            'type' => 'raw',
            'value' => 'CHtml::link("Shipments",Yii::app()->createUrl("shipments/create", array("aid"=>$data->Id)))'
        ),	

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
