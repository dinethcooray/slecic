<?php
$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<div style="background-image:url(./images/btn/backgrond.png); background-repeat:no-repeat;">
<h1>About</h1>

<p>&nbsp;&nbsp;The SRI LANKA EXPORT CREDIT INSURANCE CORPORATION (SLECIC) is committed to provide attractive and innovative Export Credit Insurance and Guarantee support services for the development of exports of Sri Lanka. The Corporation is a statutory body which operates under the Ministry of Internal & International Commerce & Food. SLECIC was created by Act No. 15 of 1978 and
commenced operation on 08 February 1979.<br />

<h3>Its main objectives are: </h3>   
<br />

   1. to issue Insurance Policies to exporters of goods and services against non receipt or delayed receipt
       of payments resulting from commercial and non-commercial risks;<br />
   2. to issue guarantees to banks and other financial institutions to facilitate the granting of pre-shipment
       and post-shipment finance; and<br />
   3. to issue guarantees to persons or institutions abroad in connection with goods and services to any
       recognised authority or institution for Sri Lanka or for the due performance of any services to be
       rendered to such persons or institutions within or outside Sri Lanka; </p><br />
<br />
</div>