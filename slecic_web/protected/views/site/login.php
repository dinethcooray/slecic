<?php
$this->pageTitle=Yii::app()->name . ' - Login';

?>
<br />
<h1>Login</h1>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
<br />
<br />
<br />

<div class="bdr" style=" padding-left:60px; width:450px; height:150px; position:relative; left:200px;"><br />

<table  width="330px" border="0">
  <tr>
    <td width="24%"><?php echo $form->labelEx($model,'username'); ?></td>
    <td width="3%">:</td>
    <td width="73%"><?php echo $form->textField($model,'username',array('size'=>'30')); ?>
		<?php echo $form->error($model,'username'); ?></td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'password'); ?></td>
    <td>:</td>
    <td><?php echo $form->passwordField($model,'password',array('size'=>'30')); ?>
		<?php echo $form->error($model,'password'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;<table width="200" style="position:relative; top:-20px" border="0">
  <tr>
    <td><?php echo $form->checkBox($model,'rememberMe'); ?></td>
    <td style="vertical-align:top"><div style="position:relative; left:-30px;"><?php echo $form->label($model,'rememberMe'); ?><?php echo $form->error($model,'rememberMe'); ?></div></td>
  </tr>
</table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;<div style="position:relative; top:-70px;"><?php echo CHtml::submitButton('Login'); ?></div></td>
  </tr>
</table>

</div><br />
<br />
<br />
<br />
<br />


<?php $this->endWidget(); ?>
</div><!-- form -->
