<div class="form">
<style> select { width: 24.6em } </style>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shipments-form',
	'enableAjaxValidation'=>false,
)); 

 $buyers = Buyers::model()->findAll(array('select'=>'Id,Name'));
 $commodities = Commodities::model()->findAll(array('select'=>'Id,Name'));
 $countries = Countries::model()->findAll(array('select'=>'Id,Name'));
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
<?php echo $form->errorSummary($model); ?>
<table width="100%" border="0">
  <tr>
    <td width="18%"><?php echo $form->labelEx($model,'Buyer_Id'); ?></td>
    <td width="43%"> : <?php echo $form->dropDownList($model,'Buyer_Id', CHtml::listData($buyers,'Id','Name'),array('empty'=>'None'));?>
		<?php echo $form->error($model,'Buyer_Id'); ?></td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'Commodity_Id'); ?></td>
    <td> : <?php echo $form->dropDownList($model,'Commodity_Id', CHtml::listData($commodities,'Id','Name'),array('empty'=>'None'));?>
		<?php echo $form->error($model,'Commodity_Id'); ?></td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'Country_Id'); ?></td>
    <td> : <?php echo $form->dropDownList($model,'Country_Id', CHtml::listData($countries,'Id','Name'),array('empty'=>'None'));?>
		<?php echo $form->error($model,'Country_Id'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;<?php echo $form->labelEx($model,'Shippment_date'); ?></td>
    <td width="34%">:<?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
  			   'model'=>$model,
                'attribute'=>'Shippment_date',
                'options'=>array(
						'changeMonth' => 'true',
						'changeYear' => 'true',
 						'showButtonPanel' => 'true', 
						'constrainInput' => 'false',
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                ),
				'htmlOptions'=>array(
    				   'style'=>'width:322px;',
			    ),
                ));
        ?>
		<?php echo $form->error($model,'Shippment_date'); ?></td>
  </tr>  
  <tr>
    <td><?php echo $form->labelEx($model,'Term_of_payment'); ?></td>
    <td> : <?php echo $form->textField($model,'Term_of_payment',array('size'=>50)); ?>
		<?php echo $form->error($model,'Term_of_payment'); ?></td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'Gross_value'); ?></td>
    <td> : <?php echo $form->textField($model,'Gross_value',array('size'=>50)); ?>
		<?php echo $form->error($model,'Gross_value'); ?></td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'Credit_duration'); ?></td>
    <td> : <?php echo $form->textField($model,'Credit_duration',array('size'=>50)); ?>
		<?php echo $form->error($model,'Credit_duration'); ?></td>
  </tr>      
  <tr>
    <td>&nbsp;</td>
    <td><?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save'); ?>&nbsp;<input name="reset" type="reset" value="Reset" /></td>
  </tr>
</table>

	<?php echo $form->hiddenField($model,'Exporter_Id',array('value'=>@$_REQUEST['aid'])); ?>

<?php
if(isset($_REQUEST['aid']))
{
		$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'shipments-grid',
		'dataProvider'=>$model->search($_REQUEST['aid']),
		'columns'=>array(
			array(
					'header'=>'Buyer',
					'class'=> 'Buyername',	
			),
			array(
					'header'=>'Commoditie',
					'class'=> 'Commoditiesname',	
			),
			'Term_of_payment',
			array(
					'header'=>'Countrie',
					'class'=> 'Countriename',	
			),
			array(
				'header'=>'Action',  
				'class'=>'CButtonColumn',
				'template'=>'{update}{delete}',
			),
		),
	)); 
}?>
<?php $this->endWidget(); ?>

</div><!-- form -->