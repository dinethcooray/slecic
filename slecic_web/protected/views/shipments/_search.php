<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Exporter_Id'); ?>
		<?php echo $form->textField($model,'Exporter_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Buyer_Id'); ?>
		<?php echo $form->textField($model,'Buyer_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Commodity_Id'); ?>
		<?php echo $form->textField($model,'Commodity_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ShipmanetValue'); ?>
		<?php echo $form->textField($model,'ShipmanetValue'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Country_Id'); ?>
		<?php echo $form->textField($model,'Country_Id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->