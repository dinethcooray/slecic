<?php
$this->breadcrumbs=array(
	'Shipments'=>array('index'),
	'Create',
);

$this->menu=array(
);
?>

<h1>Add Shipments</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>