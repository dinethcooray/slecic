<?php
$this->breadcrumbs=array(
	'Shipments'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'List Shipments', 'url'=>array('index')),
	array('label'=>'Create Shipments', 'url'=>array('create')),
	array('label'=>'Update Shipments', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Delete Shipments', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Shipments', 'url'=>array('admin')),
);
?>

<h1>View Shipments #<?php echo $model->Id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Exporter_Id',
		'Buyer_Id',
		'Commodity_Id',
		'ShipmanetValue',
		'Country_Id',
	),
)); ?>
