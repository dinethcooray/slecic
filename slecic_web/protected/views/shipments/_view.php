<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Exporter_Id')); ?>:</b>
	<?php echo CHtml::encode($data->Exporter_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Buyer_Id')); ?>:</b>
	<?php echo CHtml::encode($data->Buyer_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Commodity_Id')); ?>:</b>
	<?php echo CHtml::encode($data->Commodity_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ShipmanetValue')); ?>:</b>
	<?php echo CHtml::encode($data->ShipmanetValue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Country_Id')); ?>:</b>
	<?php echo CHtml::encode($data->Country_Id); ?>
	<br />


</div>