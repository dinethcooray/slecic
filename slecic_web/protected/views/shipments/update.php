<?php
$this->breadcrumbs=array(
	'Shipments'=>array('index'),
	$model->Id=>array('view','id'=>$model->Id),
	'Update',
);

$this->menu=array(
);
?>

<h1>Update Shipment</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>