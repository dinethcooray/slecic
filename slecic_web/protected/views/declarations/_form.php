<div class="form">
<style> select { width: 24.6em } </style>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'declarations-form',
	'enableAjaxValidation'=>false,
)); ?>

 <?php $policies = Policies::model()->findAll(array('select'=>'Id,PolicyNumber'));?>
 
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<table width="100%" border="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;<?php echo $form->labelEx($model,'PolicyId'); ?></td>
    <td>:</td>
    <td>&nbsp;<?php echo $form->dropDownList($model,'PolicyId', CHtml::listData($policies,'Id','PolicyNumber'),array('empty'=>'None'));?>
		<?php echo $form->error($model,'PolicyId'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;<?php echo $form->labelEx($model,'StampDate'); ?></td>
    <td>:</td>
    <td>&nbsp;<?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
  			   'model'=>$model,
                'attribute'=>'StampDate',
                'options'=>array(
						'changeMonth' => 'true',
						'changeYear' => 'true',
 						'showButtonPanel' => 'true', 
						'constrainInput' => 'false',
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                ),
				'htmlOptions'=>array(
    				   'style'=>'width:322px;',
			    ),
                ));
        ?>
		<?php echo $form->error($model,'StampDate'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;<?php echo $form->labelEx($model,'Status'); ?></td>
    <td>:</td>
    <td>&nbsp;<?php echo $form->textField($model,'Status',array('size'=>50)); ?>
		<?php echo $form->error($model,'Status'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;<?php echo $form->labelEx($model,'Comment'); ?></td>
    <td>:</td>
    <td>&nbsp;<?php echo $form->textField($model,'Comment',array('size'=>50)); ?>
		<?php echo $form->error($model,'Comment'); ?></td>
  </tr>  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td>&nbsp;<?php echo CHtml::submitButton($model->isNewRecord ? 'Add and add shipments' : 'Save and add shipments'); ?>&nbsp;<input name="reset" type="reset" value="Reset" /></td>
  </tr>
</table>

		<?php echo $form->hiddenField($model,'ExporterId',array('value'=>intval (Yii::app()->user->getId()))); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->