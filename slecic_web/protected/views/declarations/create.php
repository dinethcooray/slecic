<?php
$this->breadcrumbs=array(
	'Declarations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Declarations', 'url'=>array('admin')),
);
?>

<h1>Add Declarations</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>