<?php
$this->breadcrumbs=array(
	'Declarations'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Manage Declarations', 'url'=>array('admin')),
	array('label'=>'Add Declarations', 'url'=>array('create')),
	array('label'=>'Update Declarations', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Delete Declarations', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Are you sure you want to delete this item?')),

);

$policies = '';

$opolicies = Policies::model()->findByAttributes(array('Id'=>$model->PolicyId));
if(is_object($opolicies))
{
	$policies = $opolicies->PolicyNumber;
}

?>

<h1>View Declaration <?php echo $policies; ?></h1>
<div style="margin-bottom:-5px;">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		 array(
			'label'=>'Policy',
			'value'=> $policies,	
		),			
		'StampDate',
		'Comment',
		'Status',
		 array('label'=>'Shipments', 'type'=>'raw', 'value'=>CHtml::link('Shipments', array('shipments/create','aid'=>$model->Id))),		
	),
)); ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

</div>