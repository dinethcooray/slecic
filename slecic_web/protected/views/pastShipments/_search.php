<div class="wide form">
<style> select { width: 24.6em } </style>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); 

 $buyers = Buyers::model()->findAll(array('select'=>'Id,Name'));
 $countries = Countries::model()->findAll(array('select'=>'Id,Name'));
?>

	<div class="row">
		<?php echo $form->label($model,'Date'); ?>
		<?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
  			   'model'=>$model,
                'attribute'=>'Date',
                'options'=>array(
						'changeMonth' => 'true',
						'changeYear' => 'true',
 						'showButtonPanel' => 'true', 
						'constrainInput' => 'false',
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                ),
				'htmlOptions'=>array(
    				   'style'=>'width:322px;',
			    ),
                ));
        ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Buyer_Id'); ?>
		<?php echo $form->dropDownList($model,'Buyer_Id', CHtml::listData($buyers,'Id','Name'),array('empty'=>'None'));?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Country_Id'); ?>
		<?php echo $form->dropDownList($model,'Country_Id', CHtml::listData($countries,'Id','Name'),array('empty'=>'None'));?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->