<?php
$this->breadcrumbs=array(
	'Past Shipments'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Add Past Shipment', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('past-shipments-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Past Shipments</h1>

<?php echo CHtml::link('Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'past-shipments-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'Date',
		'Value',
		 array(
                'header'=>'Buyer',
				'class'=> 'Buyername',	
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
