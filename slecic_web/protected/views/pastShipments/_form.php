<div class="form">
<style> select { width: 24.6em } </style>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'past-shipments-form',
	'enableAjaxValidation'=>false,
)); ?>

 <?php 
 $buyers = Buyers::model()->findAll(array('select'=>'Id,Name'));
 $commodities = Commodities::model()->findAll(array('select'=>'Id,Name'));
 $countries = Countries::model()->findAll(array('select'=>'Id,Name'));
 ?>
 
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<table width="100%" border="0">
  <tr>
    <td width="33%">&nbsp;<?php echo $form->labelEx($model,'Date'); ?></td>
    <td width="1%">:</td>
    <td width="66%">&nbsp;<?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
  			   'model'=>$model,
                'attribute'=>'Date',
                'options'=>array(
						'changeMonth' => 'true',
						'changeYear' => 'true',
 						'showButtonPanel' => 'true', 
						'constrainInput' => 'false',
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                ),
				'htmlOptions'=>array(
    				   'style'=>'width:322px;',
			    ),
                ));
        ?>
		<?php echo $form->error($model,'Date'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;<?php echo $form->labelEx($model,'Value'); ?></td>
    <td>:</td>
    <td>&nbsp;<?php echo $form->textField($model,'Value', array('size'=>50)); ?>
		<?php echo $form->error($model,'Value'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;<?php echo $form->labelEx($model,'Buyer_Id'); ?></td>
    <td>:</td>
    <td>&nbsp;<?php echo $form->dropDownList($model,'Buyer_Id', CHtml::listData($buyers,'Id','Name'),array('empty'=>'None'));?>
		<?php echo $form->error($model,'Buyer_Id'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;<?php echo $form->labelEx($model,'Commodity_Id'); ?></td>
    <td>:</td>
    <td>&nbsp;<?php echo $form->dropDownList($model,'Commodity_Id', CHtml::listData($commodities,'Id','Name'),array('empty'=>'None'));?>
		<?php echo $form->error($model,'Commodity_Id'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;<?php echo $form->labelEx($model,'Country_Id'); ?></td>
    <td>:</td>
    <td>&nbsp;<?php echo $form->dropDownList($model,'Country_Id', CHtml::listData($countries,'Id','Name'),array('empty'=>'None'));?>
		<?php echo $form->error($model,'Country_Id'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save'); ?></td>
  </tr>
</table>

		<?php echo $form->hiddenField($model,'Exporter_Id',array('value'=>intval (Yii::app()->user->getId()))); ?>


<?php $this->endWidget(); ?>

</div><!-- form -->