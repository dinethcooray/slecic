<?php
$this->breadcrumbs=array(
	'Past Shipments'=>array('index'),
	$model->Id=>array('view','id'=>$model->Id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Past Shipment', 'url'=>array('create')),
	array('label'=>'View Past Shipment', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Manage Past Shipments', 'url'=>array('admin')),
);
?>

<h1>Update PastShipment</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>