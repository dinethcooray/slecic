<?php
$this->breadcrumbs=array(
	'Past Shipments',
);

$this->menu=array(
	array('label'=>'Create PastShipments', 'url'=>array('create')),
	array('label'=>'Manage PastShipments', 'url'=>array('admin')),
);
?>

<h1>Past Shipments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
