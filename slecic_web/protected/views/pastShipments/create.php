<?php
$this->breadcrumbs=array(
	'Past Shipments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Past Shipments', 'url'=>array('admin')),
);
?>

<h1>Add PastShipments</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>