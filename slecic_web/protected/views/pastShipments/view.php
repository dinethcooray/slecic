<?php
$this->breadcrumbs=array(
	'Past Shipments'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Add Past Shipment', 'url'=>array('create')),
	array('label'=>'Update Past Shipment', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Manage Past Shipments', 'url'=>array('admin')),
);

$buyer = Buyers::model()->findByAttributes(array('Id'=>$model->Buyer_Id))->Name;
$commodities = Commodities::model()->findByAttributes(array('Id'=>$model->Commodity_Id))->Name;
$countries = Countries::model()->findByAttributes(array('Id'=>$model->Country_Id))->Name;

?>

<h1>View PastShipment</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Date',
		'Value',						
		 array(
			'label'=>'Buyer',
			'value'=> $buyer,	
		),		
		 array(
			'label'=>'Commodity',
			'value'=> $commodities,	
		),	
		 array(
			'label'=>'Country',
			'value'=> $countries,	
		),		 
	),
)); ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
