<?php

/**
 * This is the model class for table "exporters".
 *
 * The followings are the available columns in table 'exporters':
 * @property integer $Id
 * @property string $Name
 * @property string $RegNo
 * @property string $Address
 * @property string $TelNo
 * @property string $Email
 * @property string $Description
 * @property string $ShortTag
 * @property integer $ExporterStatusId
 * @property string $MaxLiability
 * @property string $username
 * @property string $password
 * @property string $last_login_date
 * @property string $lmd
 * @property string $lmu
 */
class Exporters extends CActiveRecord
{
	public $confirmPassword;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Exporters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exporters';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Name,RegNo,Email,username,password,confirmPassword','required'),
			array('Email','email'),			
			array('ExporterStatusId', 'numerical', 'integerOnly'=>true),
			array('password', 'compare', 'compareAttribute'=>'confirmPassword'),				
			array('Name', 'length', 'max'=>100),
			array('RegNo, TelNo, Email, ShortTag, MaxLiability, username, password, lmu', 'length', 'max'=>50),
			array('Address', 'length', 'max'=>200),
			array('Description, last_login_date, lmd', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Id, Name, RegNo, Address, TelNo, Email, Description, ShortTag, ExporterStatusId, MaxLiability, username, password, last_login_date, lmd, lmu', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'Name' => 'Name',
			'RegNo' => 'Registration No',
			'Address' => 'Address',
			'TelNo' => 'Telephone No',
			'Email' => 'Email',
			'Description' => 'Description',
			'ShortTag' => 'Short Tag',
			'ExporterStatusId' => 'Exporter Status',
			'MaxLiability' => 'Max Liability',
			'username' => 'Username',
			'password' => 'Password',
			'last_login_date' => 'Last Login Date',
			'lmd' => 'Last Modified Date',
			'lmu' => 'Last Modified User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$q = @$_GET['q'];
		$criteria=new CDbCriteria;

		$criteria->compare('Id',$q, true, 'OR');
		$criteria->compare('Name',$q, true, 'OR');
		$criteria->compare('RegNo',$q, true, 'OR');
		$criteria->compare('Address',$q, true, 'OR');
		$criteria->compare('TelNo',$q, true, 'OR');
		$criteria->compare('Email',$q, true, 'OR');
		$criteria->compare('Description',$q, true, 'OR');
		$criteria->compare('ShortTag',$q, true, 'OR');
		$criteria->compare('ExporterStatusId',$q, true, 'OR');
		$criteria->compare('MaxLiability',$q, true, 'OR');
		$criteria->compare('username',$q, true, 'OR');
		$criteria->compare('password',$q, true, 'OR');
		$criteria->compare('last_login_date',$q, true, 'OR');
		$criteria->compare('lmd',$q, true, 'OR');
		$criteria->compare('lmu',$q, true, 'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}