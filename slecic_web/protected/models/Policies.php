<?php

/**
 * This is the model class for table "policies".
 *
 * The followings are the available columns in table 'policies':
 * @property integer $Id
 * @property string $PolicyNumber
 * @property double $ShipmentValue
 * @property integer $Duration
 * @property string $Status
 * @property string $RecievedDate
 * @property integer $CommodityId
 * @property string $StartDate
 * @property integer $BuyerId
 * @property integer $ExporterId
 * @property integer $CountryId
 */
class Policies extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Policies the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'policies';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Duration, CommodityId, BuyerId, ExporterId, CountryId', 'numerical', 'integerOnly'=>true),
			array('ShipmentValue', 'numerical'),
			array('PolicyNumber, Status', 'length', 'max'=>50),
			array('RecievedDate, StartDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Id, PolicyNumber, ShipmentValue, Duration, Status, RecievedDate, CommodityId, StartDate, BuyerId, ExporterId, CountryId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'PolicyNumber' => 'Policy Number',
			'ShipmentValue' => 'Shipment Value',
			'Duration' => 'Duration',
			'Status' => 'Status',
			'RecievedDate' => 'Recieved Date',
			'CommodityId' => 'Commodity',
			'StartDate' => 'Start Date',
			'BuyerId' => 'Buyer',
			'ExporterId' => 'Exporter',
			'CountryId' => 'Country',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('PolicyNumber',$this->PolicyNumber,true);
		$criteria->compare('ShipmentValue',$this->ShipmentValue);
		$criteria->compare('Duration',$this->Duration);
		$criteria->compare('Status',$this->Status,true);
		$criteria->compare('RecievedDate',$this->RecievedDate,true);
		$criteria->compare('CommodityId',$this->CommodityId);
		$criteria->compare('StartDate',$this->StartDate,true);
		$criteria->compare('BuyerId',$this->BuyerId);
		$criteria->compare('ExporterId',$this->ExporterId);
		$criteria->compare('CountryId',$this->CountryId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}