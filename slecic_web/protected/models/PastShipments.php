<?php
Yii::import('zii.widgets.grid.CGridColumn');

/**
 * This is the model class for table "pastshipments".
 *
 * The followings are the available columns in table 'pastshipments':
 * @property integer $Id
 * @property string $Date
 * @property double $Value
 * @property integer $Buyer_Id
 * @property integer $Commodity_Id
 * @property integer $Exporter_Id
 * @property integer $Country_Id
 */
class PastShipments extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PastShipments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pastshipments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(					 
			array('Date, Value, Buyer_Id, Commodity_Id, Exporter_Id, Country_Id', 'required'),
			array('Buyer_Id, Commodity_Id, Exporter_Id, Country_Id', 'numerical', 'integerOnly'=>true),
			array('Value', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Id, Date, Value, Buyer_Id, Commodity_Id, Exporter_Id, Country_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'Date' => 'Date',
			'Value' => 'Value',
			'Buyer_Id' => 'Buyer',
			'Commodity_Id' => 'Commodity',
			'Exporter_Id' => 'Exporter',
			'Country_Id' => 'Country',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('Date',$this->Date);
		$criteria->compare('Value',$this->Value);
		$criteria->compare('Buyer_Id',$this->Buyer_Id);
		$criteria->compare('Commodity_Id',$this->Commodity_Id);
		$criteria->compare('Exporter_Id',$this->Exporter_Id);
		$criteria->compare('Country_Id',$this->Country_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}


class Buyername extends CGridColumn {
 
    private $_bname = '';

    public function renderDataCellContent($row, $data) { // $row number is ignored
	
	$this->_bname = '';
	$bid = $data->Buyer_Id;
	if($bid !=0)
	{
		$buyer = Buyers::model()->findByAttributes(array('Id'=>$bid));
	   if($buyer != NULL)
		{
	   		$this->_bname=$buyer->Name;
		}
	}
        echo $this->_bname;
    }
}





