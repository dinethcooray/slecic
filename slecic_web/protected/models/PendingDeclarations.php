<?php
Yii::import('zii.widgets.grid.CGridColumn');
/**
 * This is the model class for table "declarations".
 *
 * The followings are the available columns in table 'declarations':
 * @property integer $Id
 * @property integer $ExporterId
 * @property integer $PolicyId
 * @property string $StampDate
 * @property string $Status
 * @property string $Comment
 */
class PendingDeclarations extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PendingDeclarations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'declarations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PolicyId', 'required'),
			array('ExporterId, PolicyId', 'numerical', 'integerOnly'=>true),
			array('Status', 'length', 'max'=>50),
			array('StampDate, Comment', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Id, ExporterId, PolicyId, StampDate, Status, Comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'ExporterId' => 'Exporter',
			'PolicyId' => 'Policy',
			'StampDate' => 'Stamp Date',
			'Status' => 'Status',
			'Comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$q = @$_GET['q'];
		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('ExporterId',$q, true, 'OR');
		$criteria->compare('PolicyId',$q, true, 'OR');
		$criteria->compare('StampDate',$q, true, 'OR');
		$criteria->compare('Status',$q, true, 'OR');
		$criteria->compare('Comment',$q, true, 'OR');
		$criteria->addCondition('Status="pending"');
		$criteria->order='Id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

class Policyname extends CGridColumn {
 
    private $_pname = '';

    public function renderDataCellContent($row, $data) { // $row number is ignored
	
	$this->_pname = '';
	$pid = $data->PolicyId;
	if($pid !=0)
	{
		$policyobj = Policies::model()->findByAttributes(array('Id'=>$pid));
	   if($policyobj != NULL)
		{
	   		$this->_pname=$policyobj->PolicyNumber;
		}
	}
        echo $this->_pname;
    }
}