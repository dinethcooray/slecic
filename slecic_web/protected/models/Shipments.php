<?php
Yii::import('zii.widgets.grid.CGridColumn');
/**
 * This is the model class for table "shipments".
 *
 * The followings are the available columns in table 'shipments':
 * @property integer $Id
 * @property integer $Exporter_Id
 * @property integer $Buyer_Id
 * @property integer $Country_Id
 * @property integer $Commodity_Id
 * @property double $Term_of_payment
 * @property string $Shippment_date
 * @property double $Gross_value
 * @property integer $Credit_duration
 */
class Shipments extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Shipments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shipments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Buyer_Id, Commodity_Id, Country_Id,Shippment_date', 'required'),
			array('Exporter_Id, Buyer_Id, Country_Id, Commodity_Id, Credit_duration', 'numerical', 'integerOnly'=>true),
			array('Term_of_payment, Gross_value', 'numerical'),
			array('Shippment_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Id, Exporter_Id, Buyer_Id, Country_Id, Commodity_Id, Term_of_payment, Shippment_date, Gross_value, Credit_duration', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'Exporter_Id' => 'Exporter',
			'Buyer_Id' => 'Buyer',
			'Country_Id' => 'Country',
			'Commodity_Id' => 'Commodity',
			'Term_of_payment' => 'Term Of Payment',
			'Shippment_date' => 'Shippment Date',
			'Gross_value' => 'Gross Value',
			'Credit_duration' => 'Credit Duration',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($aid)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('Exporter_Id',$this->Exporter_Id);
		$criteria->compare('Buyer_Id',$this->Buyer_Id);
		$criteria->compare('Country_Id',$this->Country_Id);
		$criteria->compare('Commodity_Id',$this->Commodity_Id);
		$criteria->compare('Term_of_payment',$this->Term_of_payment);
		$criteria->compare('Shippment_date',$this->Shippment_date,true);
		$criteria->compare('Gross_value',$this->Gross_value);
		$criteria->compare('Credit_duration',$this->Credit_duration);
		$criteria->addCondition('Exporter_Id='.$aid);
		$criteria->order='Id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

class Buyername extends CGridColumn {
 
    private $_bname = '';

    public function renderDataCellContent($row, $data) { // $row number is ignored
	
	$this->_bname = '';
	$bid = $data->Buyer_Id;
	if($bid !=0)
	{
		$buyer = Buyers::model()->findByAttributes(array('Id'=>$bid));
	  	if($buyer != NULL)
		{
	   		$this->_bname=$buyer->Name;
		}
	}
        echo $this->_bname;
    }
}

class Commoditiesname extends CGridColumn {

    private $_bname = '';

    public function renderDataCellContent($row, $data) { // $row number is ignored
	
	$this->_bname = '';
	$bid = $data->Commodity_Id;
	if($bid !=0)
	{
		$commodities = Commodities::model()->findByAttributes(array('Id'=>$bid));
	   	if($commodities != NULL)
		{
	   		$this->_bname=$commodities->Name;
		}
	}
        echo $this->_bname;
    }
}

class Countriename extends CGridColumn {
 
    private $_bname = '';

    public function renderDataCellContent($row, $data) { // $row number is ignored
	
	$this->_bname = '';
	$bid = $data->Country_Id;
	if($bid !=0)
	{
		$countries = Countries::model()->findByAttributes(array('Id'=>$bid));
	   if($countries != NULL)
		{
	   		$this->_bname=$countries->Name;
		}
	}
        echo $this->_bname;
    }
}