<?php

/**
 * This is the model class for table "buyers".
 *
 * The followings are the available columns in table 'buyers':
 * @property integer $Id
 * @property integer $CountryId
 * @property string $Name
 * @property string $City
 * @property string $ZipCode
 * @property string $Fax
 * @property string $Email
 * @property string $Address
 * @property string $TelephoneNo
 * @property string $MaxLiability
 * @property integer $BuyerStatus_Id
 */
class Buyers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Buyers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buyers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('CountryId, BuyerStatus_Id', 'numerical', 'integerOnly'=>true),
			array('Name, City, ZipCode, Fax, Email, Address, TelephoneNo, MaxLiability', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Id, CountryId, Name, City, ZipCode, Fax, Email, Address, TelephoneNo, MaxLiability, BuyerStatus_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'CountryId' => 'Country',
			'Name' => 'Name',
			'City' => 'City',
			'ZipCode' => 'Zip Code',
			'Fax' => 'Fax',
			'Email' => 'Email',
			'Address' => 'Address',
			'TelephoneNo' => 'Telephone No',
			'MaxLiability' => 'Max Liability',
			'BuyerStatus_Id' => 'Buyer Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('CountryId',$this->CountryId);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('City',$this->City,true);
		$criteria->compare('ZipCode',$this->ZipCode,true);
		$criteria->compare('Fax',$this->Fax,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('TelephoneNo',$this->TelephoneNo,true);
		$criteria->compare('MaxLiability',$this->MaxLiability,true);
		$criteria->compare('BuyerStatus_Id',$this->BuyerStatus_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}