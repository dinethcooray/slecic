﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
	public partial class SearchDirector
	{
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Director director;
        
		public SearchDirector()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}

        //text changed listner has been assigned for the each text box
        private void txtFirstName_TextChanged(object sender, TextChangedEventArgs e)
        {
            fillGrid();
        }

        private void txtLastName_TextChanged(object sender, TextChangedEventArgs e)
        {
            fillGrid();
        }

        private void txtNIC_TextChanged(object sender, TextChangedEventArgs e)
        {
            fillGrid();
        }

        private void txtEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            fillGrid();
            
            
        }

        //Filling datagrid
        private void fillGrid()
        {

            IEnumerable<Director> directors = model.Directors;
            directors = directors.Where(i => i.FirstName.Contains(txtFirstName.Text));
            directors = directors.Where(i => i.FirstName.Contains(txtLastName.Text));
            directors = directors.Where(i => i.FirstName.Contains(txtNIC.Text));
            directors = directors.Where(i => i.FirstName.Contains(txtEmail.Text));
            gridDirector.ItemsSource = directors;
        }

        //on the selection event of the data grid
        private void gridDirector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Director director =(Director) gridDirector.SelectedItem;
            ((MainWindow)Application.Current.MainWindow).frame2.Content = new ViewDirector(director);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            fillGrid();
        }

	}
}