﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace PMS
{
    /// <summary>
    /// Interaction logic for SearchExporters.xaml
    /// </summary>
    public partial class SearchExporters : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        int Id;
        public SearchExporters()
        {
            InitializeComponent();
            grdExpsearch.ItemsSource = model.Exporters;
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {


           
           

        }

        private void grdExpsearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainWindow window = (MainWindow)Application.Current.MainWindow;
            window.frame2.Content = new ViewExporter((Exporter)grdExpsearch.SelectedItem);
          
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            var exporters = model.Exporters.Where(exporter => exporter.Name.Contains(txtSearch.Text));
            grdExpsearch.ItemsSource = exporters;
        }
    }
}
