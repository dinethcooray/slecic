﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for AddPastShipmentDetails.xaml
    /// </summary>
    public partial class AddPastShipmentDetails : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        PastShipment pastShipment;
        public AddPastShipmentDetails()
        {
            InitializeComponent();
            fillComboBoxes();
            pastShipment = new PastShipment();

        }

        //fill combo boxes
        private void fillComboBoxes()
        {
            txtBuyer.ItemsSource = model.Buyers;
            txtBuyer.DisplayMemberPath = "Name";
            txtBuyer.SelectedItem = 1;

            txtExporter.ItemsSource = model.Exporters;
            txtExporter.DisplayMemberPath = "Name";
            txtExporter.SelectedItem = 1;

            txtCountry.ItemsSource = model.Countries;
            txtCountry.DisplayMemberPath = "Name";
            txtCountry.SelectedItem = 1;

            txtCommodity.ItemsSource = model.Commodities;
            txtCommodity.DisplayMemberPath = "Name";
            txtCommodity.SelectedItem = 1;
                    
        }

        //saving shipment details in the data base
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            pastShipment.Date = dtPickerDate.SelectedDate.Value;
            pastShipment.Exporter = (Exporter)txtExporter.SelectedItem;
            pastShipment.Value = Convert.ToDouble(txtValue.Text);
            pastShipment.Buyer = (Buyer)txtBuyer.SelectedItem;
            pastShipment.Commodity = (Commodity)txtCommodity.SelectedItem;
            pastShipment.Country = (Country)txtCountry.SelectedItem;
            model.AddToPastShipments(pastShipment);
            model.SaveChanges();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            dtPickerDate.ClearValue(DatePicker.SelectedDateProperty);
            txtValue.Text = "";

            

        }
    }
}
