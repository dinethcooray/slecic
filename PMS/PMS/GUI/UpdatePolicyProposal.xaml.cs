﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS.GUI
{
    /// <summary>
    /// Interaction logic for UpdatePolicyProposal.xaml
    /// </summary>
    public partial class UpdatePolicyProposal : Page
    {
        policyShipment policyShip;
        Policy policy;
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        public UpdatePolicyProposal(Policy pol)
        {
            InitializeComponent();
            this.policy = pol;
            LoadGrid();
        }

        private void LoadGrid()
        {
            if (policy != null)
            {
                IEnumerable<policyShipment> buyers = model.policyShipments;
                buyers = buyers.Where(u => u.PolicyId.Equals(policy.Id));
                grdPolicyBuyer.ItemsSource = buyers;
            }
        }
        //FILL Exporter
        private void FillExporter()
        {
            cmb_exporter.ItemsSource = model.Exporters;
            cmb_exporter.DisplayMemberPath = "Name";
        }

       

        private bool validatePolicyNumber()
        {
            if (model.Policies.Where(i => i.PolicyNumber == txt_polNo.Text).Count() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void fillDetails()
        {
          
            if (policy != null)
            {
                cmb_exporter.Text= policy.Exporter.Name;
                txt_duration.Text = policy.Duration.ToString();
                txt_polNo.Text = policy.PolicyNumber;
                txt_value.Text = policy.Value.ToString();
                dtp_start_date.SelectedDate = policy.StartDate;

            }
            
        }
          

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            
            if (cmb_exporter.SelectedItem == null)
            {
                MessageBox.Show("Please Select Exporter");
                return;
            }
            if (txt_duration.Text == "")
            {
                MessageBox.Show("Please Enter Duration");
                return;
            }
            if (txt_value.Text == "")
            {
                MessageBox.Show("Please Enter Value");
                return;
            }

            policy = model.Policies.Where(i => i.Id == policy.Id).FirstOrDefault();
            policy.Duration = Convert.ToInt32(txt_duration.Text);
            policy.Exporter = (Exporter)cmb_exporter.SelectedItem;
            policy.RecievedDate = DateTime.Now;
            policy.StartDate = dtp_start_date.SelectedDate;
            model.SaveChanges();
            MessageBox.Show("Policy Proposal Updated Successfully");
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
          
            FillExporter();
            fillDetails();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
            policy = model.Policies.Where(i => i.Id == policy.Id).FirstOrDefault();
          //  policyShip =model.policyShipments.Where(
            try
            {
                model.Policies.DeleteObject(policy);
           //     model.policyShipments.DeleteObject(
                model.SaveChanges();
                MessageBox.Show("successfully deleted the record");
            }
            catch
            {
                MessageBox.Show("Unable to delete record");
            }
        }

        private void txt_value_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
