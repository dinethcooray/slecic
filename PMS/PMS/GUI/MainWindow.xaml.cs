﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Windows.Controls.Ribbon;
using PMS.GUI;

namespace PMS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            if (UserDetails.UserId == null)
            {
                Logging lg = new Logging(this);
                this.Hide();
                lg.Show();

            }
            // Insert code required on object creation below this point.
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            frame1.Content = new SearchDeclaration();
            frame2.Content = null;
         
        }

        private void LayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //frame1.SetValue(WidthProperty, frame1.GetValue(Grid.WidthProperty));
        }

        private void RibbonButton_Click_4(object sender, RoutedEventArgs e)
        {
            frame1.Content = new ViewUser();
            frame2.Content = null;
        }

        private void RibbonButton_Click_5(object sender, RoutedEventArgs e)
        {
            frame1.Content = new AddUser();
            frame2.Content = null;
        }

        private void RibbonButton_Click_6(object sender, RoutedEventArgs e)
        {
            frame1.Content = new AddAccessToUser();
            frame2.Content = null;
        }

        private void btnAddGroup_Click(object sender, RoutedEventArgs e)
        {
            frame1.Content = new AddAccessGroup();
            frame2.Content = null;
        }

        private void btnAssignAccess_Click(object sender, RoutedEventArgs e)
        {
            frame1.Content = new AddAccessToGroup();
            frame2.Content = null;
        }

        private void RibbonButton_Click(object sender, RoutedEventArgs e)
        {
            frame1.Content = new SearchPolicyProposal();
            frame2.Content = null;
        }

        private void RibbonButton_Click_14(object sender, RoutedEventArgs e)
        {
            frame1.Content = new AddPolicyProposal();
            frame2.Content = null;
        }

        private void RibbonButton_Click_12(object sender, RoutedEventArgs e)
        {
            frame1.Content = new SearchDeclaration();
            frame2.Content = null;
        }

        private void RibbonButton_Click_13(object sender, RoutedEventArgs e)
        {
            frame1.Content = new AddDeclaration();
            frame2.Content = null;
        }

        private void RibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            //search exporter
            frame1.Content = new SearchExporters();
            frame2.Content = null;
        }

        private void RibbonButton_Click_2(object sender, RoutedEventArgs e)
        {
            //add exporter
            frame1.Content = new AddExporter();
            frame2.Content = null;
        }

        private void RibbonButton_Click_3(object sender, RoutedEventArgs e)
        {
            //search director
            frame1.Content = new SearchDirector();
            frame2.Content = null;
        }

        private void RibbonButton_Click_7(object sender, RoutedEventArgs e)
        {
            //add director
            frame1.Content = new AddDirector();
            frame2.Content = null;
        }

        private void RibbonButton_Click_8(object sender, RoutedEventArgs e)
        {
            //search shipment
            frame1.Content = new ViewPastShipment();
            frame2.Content = null;
        }

        private void RibbonButton_Click_9(object sender, RoutedEventArgs e)
        {
            //add shipment
            frame1.Content = new AddPastShipmentDetails();
            frame2.Content = null;
        }

        private void RibbonButton_Click_10(object sender, RoutedEventArgs e)
        {
            //search ind buyer
            frame1.Content = new SearchIndividualBuyer();
            frame2.Content = null;
        }

        private void RibbonButton_Click_11(object sender, RoutedEventArgs e)
        {
            //add ind buyer
            frame1.Content = new AddIndividualBuyer();
            frame2.Content = null;
        }

        private void RibbonButton_Click_15(object sender, RoutedEventArgs e)
        {
            //search com buyer
            frame1.Content = new SearchCompanyBuyer();
            frame2.Content = null;
        }

        private void RibbonButton_Click_16(object sender, RoutedEventArgs e)
        {
            //add com buyer
            frame1.Content = new AddCompanyBuyer();
            frame2.Content = null;
        }

        private void RibbonButton_Click_17(object sender, RoutedEventArgs e)
        {
            frame1.Content = new AddPremiumRates();
            frame2.Content = null;
        }

        private void btnAddGroup_Copy_Click(object sender, RoutedEventArgs e)
        {
            frame1.Content = new ActivityLog();
            frame2.Content = null;
        }

        private void RibbonButton_Click_Email_Sender(object sender, RoutedEventArgs e)
        {
            frame1.Content = new EmailSender();
            frame2.Content = null;

        }
        
    }
}
