﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for CompanyBuyerDetails.xaml
    /// </summary>
    public partial class CompanyBuyerDetails : Page
    {
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        CompanyBuyer companybuyer;
        public CompanyBuyerDetails(CompanyBuyer cmbuyer)
        {
            if (cmbuyer != null)
            {
                InitializeComponent();

                this.companybuyer = cmbuyer;
            }
        }
        private void fillDetails()
        {
            if (companybuyer != null)
            {
                lblCAddress.Content = companybuyer.Address;
                lblCCity.Content = companybuyer.City;
                lblCCountry.Content = companybuyer.Country.Name;
                lblCEmail.Content = companybuyer.Email;
                lblCFax.Content = companybuyer.Fax;
                lblCName.Content = companybuyer.Name;
                lblCTelephoneNo.Content = companybuyer.TelephoneNo;
                lblCZipcode.Content = companybuyer.ZipCode;
                lblShortName.Content = companybuyer.ShortName;
                lblCreg.Content = companybuyer.RegNo;
                lbCMaxLiability.Content = companybuyer.MaxLiability;
                lblCStatus.Content = companybuyer.BuyerStatus.description;
            }
        }
        private void btnCUpdate_Click(object sender, RoutedEventArgs e)
        {

            if (companybuyer != null)
            {
                MainWindow window = (MainWindow)Application.Current.MainWindow;
                window.frame2.Content = new UpdateComapanyBuyerDetails(this.companybuyer);
            } 

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Page_Loaded_1(object sender, RoutedEventArgs e)
        {
            fillDetails();
        }

        private void btnCDelete_Click(object sender, RoutedEventArgs e)
        {
            if (companybuyer != null)
            {
                companybuyer = (CompanyBuyer)model.Buyers.Where(i => i.Id == companybuyer.Id).FirstOrDefault();
                
                try
                {
                    model.Buyers.DeleteObject(companybuyer);
                    model.SaveChanges();
                    MessageBox.Show("Delete the company buyer succesfully");

                }
                catch
                {
                    MessageBox.Show("Unable to delete record");
                }
            }
        }
    }
}
