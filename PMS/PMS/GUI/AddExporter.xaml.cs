﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS
{
    /// <summary>
    /// Interaction logic for AddExporter.xaml
    /// </summary>
    public partial class AddExporter : Page
    {
        PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Exporter exporter;
        public AddExporter()
        {
            InitializeComponent();
            exporter = new Exporter();

            //fill combo boxes
            txtStatus.ItemsSource = model.ExporterStatus;
            txtStatus.DisplayMemberPath = "Description";
            txtStatus.SelectedItem = 1;
        }

        public AddExporter(int ExpId)
        { 
            //create a exporter object if the exp id is given
           exporter=(Exporter)model.Exporters.Where(exp=>exp.Id==ExpId).First();
           txtName.Text = exporter.Name;
           txtRegNo.Text = exporter.RegNo;
           txtShortTag.Text=exporter.ShortTag;
           txtTelNo.Text = exporter.TelNo;
           txtEmail.Text = exporter.Email;
           txtDescription.Text = exporter.Description;
           txtAddress.Text = exporter.Address;
            

        }

        private void button_Save_Click(object sender, RoutedEventArgs e)
        {
            Validation validation = new Validation();
            IEnumerable<Exporter> exporters = model.Exporters.Where(exp => exp.RegNo == txtRegNo.Text);
            //IEnumerable<User> users = model.Users.Where(user => user.UserName == txtUserName.Text);
            if (exporters.Count() == 0)
            {
                //validation for emails
                if (validation.ValidateTextField(txtEmail.Text) ? validation.ValidateEmail(txtEmail.Text) : true)
                {

                    exporter.Name = txtName.Text;
                    exporter.RegNo = txtRegNo.Text;
                    exporter.TelNo = txtTelNo.Text;
                    exporter.ShortTag = txtShortTag.Text;
                    exporter.Email = txtEmail.Text;
                    exporter.Description = txtDescription.Text;
                    exporter.Address = txtAddress.Text;
                    exporter.ExporterStatu=(ExporterStatus)txtStatus.SelectedItem ;
                    //adding to data model
                    model.AddToExporters(exporter);
                    //applying changes to the database
                    model.SaveChanges();
                    MessageBox.Show("Exporter added successfully", "Added");


                    
                }
                else
                {
                    MessageBox.Show("Invalid Email");
                }
            }
        }

        private void txtStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
