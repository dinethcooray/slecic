﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace PMS
{
	public partial class ViewExporter
	{
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        private Exporter exporter;
		public ViewExporter()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}
        public ViewExporter(Exporter exp)
        {
            if (exp != null)
            {
                this.InitializeComponent();
                this.exporter = exp;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_Update(object sender, RoutedEventArgs e)
        {
            
            ((MainWindow)Application.Current.MainWindow).frame2.Content = new EditExporter(exporter);

        }

        private void Button_Click_Remove(object sender, RoutedEventArgs e)
        {
            var dir = model.Exporters.Where(i => i.Id == exporter.Id).FirstOrDefault();
            model.Exporters.DeleteObject(dir);
            model.SaveChanges();


        }

        private void page_loaded(object sender, RoutedEventArgs e)
        {
            //fill details
            if (exporter != null)
            {
                lblName.Content = exporter.Name;
                lblAddress.Content = exporter.Address;
                lblRegNo.Content = exporter.RegNo;
                lblShortTag.Content = exporter.ShortTag;
                lblStatus.Content = exporter.ExporterStatu.Description;
                lblDescription.Content = exporter.Description;
                lblEmail.Content = exporter.Email;
                lblTelNo.Content = exporter.TelNo;

                //datasource to the datagrid
                gridDirectors.ItemsSource = exporter.Directors;

            }
        }

 

        private void gridDirectors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
	}
}