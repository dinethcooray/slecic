﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PMS
{
	public partial class EditDirector
	{
        private PMS_Main_ModelContainer model = new PMS_Main_ModelContainer();
        Director director;
		public EditDirector()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}
        public EditDirector(Director dir)
        {
            director = dir;
            InitializeComponent();
        }

        private void fillDetails()
        {
            txtAddress.Text = director.Address;
            txtEmail.Text = director.Email;
            txtFirstName.Text = director.FirstName;
            txtLastName.Text = director.LastName;
            txtNIC.Text = director.NIC;
            txtPassportNo.Text = director.PassportNo;
            txtTelNo.Text = director.TelNo;
            dtPickerBirthDay.SelectedDate = director.DateOfBirth;
            dtPickerJoinedDate.SelectedDate = director.JoinedDate;
        
        }
      
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
              director.Address=txtAddress.Text;
              director.Email =txtEmail.Text;
              director.FirstName =txtFirstName.Text;
              director.LastName =txtLastName.Text;
              director.NIC =txtNIC.Text;
              director.PassportNo =txtPassportNo.Text;
              director.TelNo =txtTelNo.Text;
              director.DateOfBirth =dtPickerBirthDay.SelectedDate;
              director.JoinedDate =dtPickerJoinedDate.SelectedDate;

              model.SaveChanges();

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void page_loaded(object sender, RoutedEventArgs e)
        {
            fillDetails();
        }
	}
}