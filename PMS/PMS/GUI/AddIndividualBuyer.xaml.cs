﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PMS
{
	public partial class AddIndividualBuyer
	{
        PMS_Main_ModelContainer model;
		public AddIndividualBuyer()
		{
			this.InitializeComponent();
            model = new PMS_Main_ModelContainer();
			// Insert code required on object creation below this point.
		}

     /*   private void btnIAdd_Click(object sender, RoutedEventArgs e)
        {
           
        }
*/
        private void BindCountry()
        {
            var countries = model.Countries;
            comboxICountry.ItemsSource = countries;
            comboxICountry.DisplayMemberPath = "Name";
            comboxICountry.ItemsSource = countries;
            comboxICountry.DisplayMemberPath = "Name";
        }
        private void BindStatus()
        {
            var status = model.BuyerStatus;
            comboIStatus.ItemsSource = status;
            comboIStatus.DisplayMemberPath = "description";

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BindCountry();
            BindStatus();

        }

        private void btnIAdd_Click_1(object sender, RoutedEventArgs e)
        {
            IndividualBuyer indBuyer = new IndividualBuyer();

            String error = "";
            // set properties to indiviudaBuyer object
            indBuyer.Name = txtIFName.Text;
            indBuyer.MobileNo = txtIMobileNo.Text;
            indBuyer.TelephoneNo = txtItelphone.Text;
            indBuyer.NIC = txtINic.Text;
            if(birthdayPicker.SelectedDate!=null)
            indBuyer.DateOfBirth = birthdayPicker.SelectedDate.Value;
            indBuyer.Country = (Country)comboxICountry.SelectedItem;

            indBuyer.City = txtICity.Text;
            indBuyer.ZipCode = txtIZipCode.Text;
            indBuyer.Address = txtIAddress.Text;
            indBuyer.Passport = txtIPassport.Text;
            indBuyer.Email = txtIEmail.Text;
            indBuyer.Fax = txtIFax.Text;
            indBuyer.MaxLiability = txtILiability.Text;

            indBuyer.BuyerStatus = (BuyerStatus)comboIStatus.SelectedItem;

            // model.AddToBuyers(indBuyer);
           

            Validation v = new Validation();// validation class is used to validate in relevant way
            if (!v.ValidateTextField(txtIFName.Text))
                error = "Enter full name,";
            if (!v.ValidateTextField(txtIAddress.Text))
                error = error + "Enter address,";
            if (!v.ValidateTextField(txtIEmail.Text))
                error = error + "Enter Email address,";
            if (txtIEmail.Text != "")
            {
                if (!v.ValidateEmail(txtIEmail.Text))
                    error = error + "Enter valide email,";
                
            }




            if (comboIStatus.SelectedItem == null)
                error = error + "select status,";

            if (comboxICountry.SelectedItem == null)
                error = error + "Select the country,";

                    if (error == "")
                    {
                        model.AddToBuyers(indBuyer);
                        model.SaveChanges();
                        MessageBox.Show("Added to the database successfully");

                    }
                    else
                    {
                        MessageBox.Show(error, "Error occured ");

                    }



                    //if (v.ValidateTextField(txtIFName.Text))
                    //{
                    //    if(v.ValidateEmail(txtIEmail.Text))
                    //    {
                    //        model.AddToBuyers(indBuyer);
                    //        model.SaveChanges();
                    //        MessageBox.Show("Added to the database successfully");
                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("Incorrect email or need to enter email", "Error occured in Email");

                    //    }




                    //}
                    //else
                    //{
                    //    MessageBox.Show("Need to enter First name and last name", "Error");

                    //}

                }
            
        
            

       

        private void btnIClear_Click(object sender, RoutedEventArgs e)
        {
             txtIFName.Text="";
             txtIMobileNo.Text="";
             txtItelphone.Text="";
             txtINic.Text="";
             txtICity.Text="";
            txtIZipCode.Text="";
            txtIAddress.Text="";
            txtIPassport.Text="";
             txtIEmail.Text="";
            txtIFax.Text="";
             txtILiability.Text="";
        }
	}
}