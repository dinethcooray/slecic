﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PMS
{
    class UserDetails
    {
        /// <summary>
        /// Static value protected by access routine.
        /// </summary>
        static string _userId;

        /// <summary>
        /// Access routine for global variable.
        /// </summary>
        public static string UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }
    }
}
